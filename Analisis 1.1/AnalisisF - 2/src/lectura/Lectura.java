/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lectura;

import clases.Contacto;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 *
 * @author luisg
 */
public class Lectura {

    public void lectura() throws IOException {
        int contador = 20;
        //cadena que contendra las lineas que vamos leyendo del archivo
        String cadenaLeida = "";
        //lista donde iremos agregando cada uno de los objetos tipo contacto
        LinkedList<Contacto> contactos = new LinkedList<Contacto>();
        FileReader fr;

        try {
            fr = new FileReader("contacts.vcf");
            BufferedReader archivoLectura = new BufferedReader(fr);

            cadenaLeida = archivoLectura.readLine();
            //System.out.println(cadenaLeida);
            String cad = "";

            while (cadenaLeida != null) {
                //while (contador >= 0) {

                String cadenaAux;

                StringTokenizer st = new StringTokenizer(cadenaLeida, ":");
                //etiqueta BEGIN
                cadenaAux = st.nextToken();

                if (cadenaAux.equals("BEGIN")) {
                    System.out.println("Etiqueta:" + cadenaAux);
                    //obtenemos su atributo
                    cadenaAux = st.nextToken();
                    System.out.println("Atributo:" + cadenaAux);

                    //recuperamos la siguiente linea
                    cadenaLeida = archivoLectura.readLine();
                    StringTokenizer st2 = new StringTokenizer(cadenaLeida, ":");
                    //recuperamos el elemento
                    cadenaAux = st2.nextToken();

                } else if (cadenaAux.equals("VERSION")) {
                    System.out.println("Etiqueta:" + cadenaAux);
                    //pasamos al siguiente que sera el atributo
                    cadenaAux = st.nextToken();
                    System.out.println("Atributo:" + cadenaAux);
                    //recuperamos la siguiente linea
                    cadenaLeida = archivoLectura.readLine();
                    StringTokenizer st3 = new StringTokenizer(cadenaLeida, ":");
                    //recuperamos el elemento
                    cadenaAux = st3.nextToken();

                } else if (cadenaAux.equals("N")) {
                    System.out.println("Etiqueta:" + cadenaAux);
                    //pasamos al siguiente que sera el atributo
                    cadenaAux = st.nextToken();
                    //System.out.println("Cadena total:" + cadenaAux);
                    //dividimos en cadenas separadas por el identificador ;
                    String[] split = cadenaAux.split(";");
                    //System.out.println("Lognitud de la linea hasta la ultima palabra:" + split.length);
                    //contador total de esta linea
                    int cont1 = 5;
                    for (int i = 0; i < split.length; i++) {
                        //con este if solo mostramos los espacios vacias hasta que encuntre una cadena
                        if (split[i].equals("")) {
                            System.out.println("Atributo:Vacio");
                            //reducimos un campo de los faltantes (5 totales)
                            cont1 -= 1;
                        } else {
                            System.out.println("Atributo:" + split[i]);
                            //reducimos un campo de los faltantes (5 totales)
                            cont1 -= 1;
                        }
                    }
                    //por ultimo agregamos todos los campos faltantes
                    for (int j = 0; j < cont1; j++) {
                        System.out.println("Atributo: Vacio");
                    }
                    //avanzamos
                    cadenaLeida = archivoLectura.readLine();
                    StringTokenizer st4 = new StringTokenizer(cadenaLeida, ":");
                    //recuperamos el elemento
                    cadenaAux = st4.nextToken();

                } else if (cadenaAux.equals("FN")) {
                    System.out.println("Etiqueta:" + cadenaAux);
                    //pasamos al siguiente que sera el atributo
                    cadenaAux = st.nextToken();
                    System.out.println("Atributo:" + cadenaAux);
                    //recuperamos la siguiente linea
                    cadenaLeida = archivoLectura.readLine();
                    StringTokenizer st2 = new StringTokenizer(cadenaLeida, ";");
                    //recuperamos el elemento
                    cad = st2.nextToken();
                }
                if (cad.equals("TEL")) {

                    System.out.println("Etiqueta:" + cad);
                    //pasamos al siguiente que sera el atributo
                    StringTokenizer st2 = new StringTokenizer(cadenaLeida, ":");
                    cad = st2.nextToken();
                    //remplazamos el etiqueta antigua por la cadena vacia
                    cad = cad.replace("TEL;", "");
                    System.out.println("Tipo de Telefono:" + cad);
                    cad = st2.nextToken();
                    System.out.println("Atributo:" + cad);

                    //avanzmos
                    cadenaLeida = archivoLectura.readLine();
                    StringTokenizer st7 = new StringTokenizer(cadenaLeida, ";");
                    //recuperamos el elemento
                    cad = st7.nextToken();//ESTA CADENA ES LA QUE CONTINUARA DE AQUI EN ADELANTE
                    System.out.println("Atributo:" + cad);

                }
                if (cad.equals("EMAIL")) {

                    System.out.println("Etiqueta:" + cad);
                    //pasamos al siguiente que sera el atributo
                    StringTokenizer st2 = new StringTokenizer(cadenaLeida, ":");
                    cad = st2.nextToken();
                    //remplazamos el etiqueta antigua por la cadena vacia
                    cad = cad.replace("EMAIL;", "");
                    System.out.println("Tipo de EMAIL:" + cad);
                    cad = st2.nextToken();
                    System.out.println("Atributo:" + cad);

                    //avanzmos
                    cadenaLeida = archivoLectura.readLine();
                    StringTokenizer st7 = new StringTokenizer(cadenaLeida, ";");
                    //recuperamos el elemento
                    cad = st7.nextToken();

                }
                if (cad.equals("ADR")) {
                    System.out.println("Etiqueta:" + cad);
                    //pasamos al siguiente que sera el atributo
                    StringTokenizer st1 = new StringTokenizer(cadenaLeida, ":");
                    cad = st1.nextToken();
                    //remplazamos el etiqueta antigua por la cadena vacia
                    cad = cad.replace("ADR;", "");
                    System.out.println("Tipo de DIRECCION:" + cad);
                    cad = st1.nextToken();
                    System.out.println("Atributo:" + cad);

                    //dividimos en cadenas separadas por el identificador ;
                    String[] split = cad.split(";");
                    //System.out.println("Lognitud de la linea hasta la ultima palabra:" + split.length);
                    //contador total de esta linea
                    int cont1 = 7;
                    for (int i = 0; i < split.length; i++) {
                        //con este if solo mostramos los espacios vacias hasta que encuntre una cadena
                        if (split[i].equals("")) {
                            System.out.println("Atributo:Vacio");
                            //reducimos un campo de los faltantes (5 totales)
                            cont1 -= 1;
                        } else {
                            System.out.println("Atributo:" + split[i]);
                            //reducimos un campo de los faltantes (5 totales)
                            cont1 -= 1;
                        }
                    }
                    //por ultimo agregamos todos los campos faltantes
                    for (int j = 0; j < cont1; j++) {
                        System.out.println("Atributo:  Vacio");
                    }
                    //avanzamos
                    cadenaLeida = archivoLectura.readLine();
                    StringTokenizer st4 = new StringTokenizer(cadenaLeida, ";");
                    //recuperamos el elemento
                    cad = st4.nextToken();

                }
                if (cad.equals("PHOTO")) {
                    String encod;
                    System.out.println("Etiqueta:" + cad);
                    //pasamos al siguiente que sera el atributo
                    StringTokenizer st8 = new StringTokenizer(cadenaLeida, ":");
                    //en este momento toma la cadena desde FOTO por eso se avanza 2 veces
                    encod = st8.nextToken();

                    System.out.println("BASE:" + encod);
                    //pasamos al siguiente que sera el atributo
                    cad = st8.nextToken();

                    //avanzamos
                    String nuevo = "";

                    while (!cad.endsWith("Z=")) {
                        nuevo += cad.trim() + "";
                        cad = archivoLectura.readLine();
                    }
                    cad += archivoLectura.readLine();

                    System.out.println("CADENA FOTO:" + nuevo);
                    
                    System.out.println(nuevo.length());
                 

                    nuevo = nuevo + cad;

                    StringTokenizer st11 = new StringTokenizer(cadenaLeida, ":");
                    //en este momento toma la cadena desde FOTO por eso se avanza 2 veces
                    encod = st11.nextToken();
                    System.out.println("CADENA FOTO:" + nuevo.length());

                    //avanzmos
                    cadenaLeida = archivoLectura.readLine();
                    StringTokenizer st7 = new StringTokenizer(cadenaLeida, ";");
                    //recuperamos el elemento
                    cad = st7.nextToken();

                    System.out.println("Cadena Final:" + cad);

                }
                if (cad.equals("END:VCARD")) {
                    
                    //avanzmos
                    cadenaLeida = archivoLectura.readLine();
                    StringTokenizer st15 = new StringTokenizer(cadenaLeida, ":");
                    //recuperamos el elemento
                    cad = st15.nextToken();
                    cadenaAux = cad;

                    System.out.println("Cadena Final:" + cad);
                    //avanzamos
                    cadenaLeida = archivoLectura.readLine();
                    StringTokenizer st4 = new StringTokenizer(cadenaLeida, ";");
                    cad = st4.nextToken();
                    cadenaAux = cad;

                }

            

        }

        archivoLectura.close();
    }
    catch (FileNotFoundException e

    
        ) {
            System.out.println("No se pudo encontrar el archivo");
        e.printStackTrace();
    }
    catch (IOException e

    
        ) {
            System.out.println("No se pudo leer del archivo");
        e.printStackTrace();
    }
    catch (NoSuchElementException e

    
    
    ) {
            //por si no encuetra con el tokenizer solo pasamos
        } catch (NullPointerException e

    
    

) {
            //por si no encuetra con el tokenizer solo pasamos
        }

    }

}
