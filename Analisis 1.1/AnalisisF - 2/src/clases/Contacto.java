package clases;

import java.util.LinkedList;

/**
 *
 * @author luisg
 */
public class Contacto {

    @Override
    public String toString() {
        return "Contacto{" + "idContacto=" + idContacto + ", apellido=" + apellido + ", nombre=" + nombre + ", alias=" + alias + ", numeros=" + numeros + ", correos=" + correos + ", direcciones=" + direcciones + ", foto=" + foto + '}';
    }
    
    private String idContacto;
    private String apellido;
    private String nombre;
    private String alias;
    private LinkedList<NumTelefonico> numeros;
    private LinkedList<Correo> correos;
    private LinkedList<Direccion> direcciones;
    private Foto foto;

    public String getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(String idContacto) {
        this.idContacto = idContacto;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public LinkedList<NumTelefonico> getNumeros() {
        return numeros;
    }

    public void setNumeros(LinkedList<NumTelefonico> numeros) {
        this.numeros = numeros;
    }

    public LinkedList<Correo> getCorreos() {
        return correos;
    }

    public void setCorreos(LinkedList<Correo> correos) {
        this.correos = correos;
    }

    public LinkedList<Direccion> getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(LinkedList<Direccion> direcciones) {
        this.direcciones = direcciones;
    }

    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }
}
