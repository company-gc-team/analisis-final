
package clases;

import java.util.LinkedList;

/**
 *
 * @author luisg
 */
public class Correo {
    private String etiqueta;
    private String correo;

    @Override
    public String toString() {
        return "Correo{" + "etiqueta=" + etiqueta + ", correo=" + correo + '}';
    }
    
    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
   
}
