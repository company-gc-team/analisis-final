/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manejador;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luisg
 */
public class SqlHelperContact {
    private SqlConn conn;
    
    //constructor para inicializar el objeto con una nueva conexion
    public SqlHelperContact() throws IOException{
        conn = new SqlConn();   
    }
    
    //metodo para recuperar los datos de la base de datos usando la conexion
    public void imprime() throws SQLException{
        //Statement st;
        //ResultSet rs;
        
        try {
            conn.connect();
            //query que hara la consulta
            String query = "SELECT * FROM cliente;";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);
            ResultSet result = statement.executeQuery(query);
            while(result.next()){
                System.out.println(result.getString("nombre_cliente"));
            }
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SqlHelperContact.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
