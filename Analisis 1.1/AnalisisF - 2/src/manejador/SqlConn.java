/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manejador;

import java.io.FileReader;

//import com.sun.jdi.connect.spi.Connection;
import com.mysql.jdbc.Connection;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
//import java.lang.System.Logger;
//import java.lang.System.Logger.Level;
import java.util.Properties;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author luisg
 */
public class SqlConn {
    //Declaramos las variables
    private Connection conn;
    private String stringConn;
    
    private Properties configuration;
    
    //con este metodo recuperamos la conexion
    public SqlConn() throws FileNotFoundException, IOException{
        
        //String path = System.getProperty("user.dir")+System.getProperty("file.separator");
        //FileInputStream fi = new FileInputStream(path+"configuration.properties");
        //configuration = new Properties();
        Properties prop = new Properties();
        FileReader reader = new FileReader("src\\properties.properties");
        prop.load(reader);
        //leemos el archivo
        //configuration.load(fi);
        
        //Recuperamos cada uno de los datos del archivo properties
        String host = prop.getProperty("host");
        System.out.println(host);
        String port = prop.getProperty("port");
        System.out.println(port);
        String dbName = prop.getProperty("dbName");
        System.out.println(dbName);
        String user = prop.getProperty("user");
        System.out.println(user);
        String password = prop.getProperty("password");
        System.out.println(password);
        
        
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("jdbc:mysql://");
        strBuilder.append(host);
        strBuilder.append(":");
        strBuilder.append(port);
        strBuilder.append("/");
        strBuilder.append(dbName);
        strBuilder.append("?user=");
        strBuilder.append(user);
        strBuilder.append("&password=");
        strBuilder.append(password);
        
        //le mandamos a la conexion toda la cadena 
        stringConn = strBuilder.toString();
        System.out.println(strBuilder.toString());

      
    }
    
    //metodo para desconectar
    
    public boolean connect() throws ClassNotFoundException{
     
        Class.forName("com.mysql.jdbc.Driver");
        try {
            conn = (Connection) DriverManager.getConnection(stringConn);
            return true;
            /*
            }catch(SQLException ex){
            Logger.getLogger(SqlHelperCandidate.class.getName()).log(Level.SEVERE, null, ex);  
            return false;
            }*/
        } catch (SQLException ex) {
            Logger.getLogger(SqlConn.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
            
    }
    
    public boolean disonnect(){
      
        try {
            conn.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SqlConn.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
       
        
    }
    
    public Connection getCoon(){
        return conn;
    }
}
