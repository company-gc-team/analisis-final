/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analisisf;

import clases.Contacto;
import clases.Correo;
import clases.Direccion;
import clases.Foto;
import clases.NumTelefonico;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import lectura.Lectura;
import manejador.SqlHelperContact;
import manejador.SqlHelperContact2;



/**
 *
 * @author luisg
 */
public class AnalisisF {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        // TODO code application logic here
        Lectura lectura = new Lectura();
        lectura.lectura();
        
        /*
       
        //CREAMOS UN CONTACTO
        Contacto contacto = new Contacto();
        
        //INICIALIZAMOS EL OBJETO CREADO
        contacto.setApellido("Cruz Estrada");
        contacto.setNombre("Luis Guillermo");
        contacto.setAlias(contacto.getNombre()+" " +contacto.getApellido()+"\n");
        //System.out.print(contacto.getAlias());
        //----------TELEFONOS
        //CREAMOS UNA LISTA DE NUMEROS VACIA
        LinkedList<NumTelefonico> numeros = new LinkedList<NumTelefonico>();
        //CREAMOS UNA INSTANCIA DE LA CLASE NUMERO
        NumTelefonico num1 = new NumTelefonico();
        //ATRIBUTOS PARA AGREGAR
        String tel1 = "58111346";
        String etiqueta1 = "TEL;CELL:";
        //AGREGAMOS LOS ELEMENTOS A LA CLASE
        num1.setEtiqueta(etiqueta1);
        num1.setNumero(tel1);
        //Agregamos el telefono a la lista
        numeros.add(num1);
        //imprimimos el segundo elemento de la lista
        //System.out.println(numeros.get(0));
        //CREAMOS UNA INSTANCIA DE LA CLASE NUMERO
        NumTelefonico num2 = new NumTelefonico();
        //ATRIBUTOS PARA AGREGAR
        String tel2 = "+5510522522";
        String etiqueta2 = "TEL;WORK:";
        num2.setEtiqueta(etiqueta2);
        num2.setNumero(tel2);
        //Agregamos el telefono a la lista
        numeros.add(num2);
        //imprimimos el segundo elemento de la lista
        //System.out.println(numeros.get(1));
        //-----------CORREOS
        //CREAMOS UNA LISTA DE CORREOS
        LinkedList<Correo> correos = new LinkedList<Correo>();
        //CREAMOS UNA INSTANCIA DE LA CLASE NUMERO
        Correo correo1 = new Correo();
        //ATRIBUTOS PARA AGREGAR
        String etiCorr1 = "EMAIL;HOME:";
        String corr1 = "luis@gmail.com";
        correo1.setEtiqueta(etiCorr1);
        correo1.setCorreo(corr1);
        correos.add(correo1);
        //System.out.println(correos.get(0));
        //Segundo correo----------
        Correo correo2 = new Correo();
        //ATRIBUTOS PARA AGREGAR
        String etiCorr2 = "EMAIL;WORK:";
        String corr2 = "al2183033698@azc.uam.mx";
        correo2.setEtiqueta(etiCorr2);
        correo2.setCorreo(corr2);
        correos.add(correo2);
        //System.out.println(correos.get(1));
        //-----------DIRECCIONES
        //CREAMOS UNA LISTA DE DIRECCIONES VACIA
        LinkedList<Direccion> direcciones = new LinkedList<Direccion>();
        //CREAMOS UNA INSTANCIA DE LA CLASE NUMERO
        Direccion direccion = new Direccion();
        //ATRIBUTOS PARA AGREGAR
        String calle = "28 de Octubre 23";
        String colonia = "Acopilco";
        String estado = "";
        String cp = "";
        String pais = "";
        String etiDir1 = "ADR;HOME";
        //AGREGAMOS LOS ELEMENTOS A LA CLASE
        direccion.setEtiqueta(etiDir1);
        direccion.setCalle(calle);
        direccion.setColonia(colonia);
        direccion.setEstado(estado);
        direccion.setCp(cp);
        direccion.setPais(pais);
        //Agregamos el correo a la lista
        direcciones.add(direccion);
        //imprimimos el segundo elemento de la lista
        //System.out.println(direcciones.get(0));
        //-----------Foto
        //CREAMOS UNA INSTANCIA DE LA CLASE FOTO
        Foto foto = new Foto();
        foto.setCadena("/9j/4AAQSkZJRgABAQAAAQABAAD/4gIoSUNDX1BST0ZJTEU\n" +
" AAQEAAAIYAAAAAAIQAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAA\n" +
" AAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        foto.setEqtiqueta("PHOTO;ENCODING=BASE64;JPEG:");
        //System.out.println(foto);
        //------TERMINANDO DE INICIALIZAR EL OBJETO CONTACTO CON LOS DATOS QUE SE CREARON
        contacto.setNumeros(numeros);
        contacto.setCorreos(correos);
        contacto.setDirecciones(direcciones);
        contacto.setFoto(foto);
        contacto.setIdContacto("1");
        //System.out.println(contacto);
        //Imprimimos el segundo telefono del contacto
        //System.out.println(contacto.getNumeros().get(1));
        //Imprimimos el primer correo del contacto
        //System.out.println(contacto.getCorreos().get(0));
        
        
        
        SqlHelperContact2 a = new SqlHelperContact2();
        System.out.println("Ingresando un Contacto...");
        a.insertarContacto(contacto);
        
        a.imprimeContacto();
        */
        
        
    }
   
}
