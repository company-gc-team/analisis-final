/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analisisf;

import clases.Contacto;
import escritura.Escritura;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import lectura.Lectura;
import manejador.SqlHelperContact2;

/**
 *
 * @author luisg
 */
public class ControladorAnalisis {

    public LinkedList<Contacto> lectura(String ruta) throws IOException {
        Lectura lectura = new Lectura();

        //agregando ID a los contactos
        LinkedList<Contacto> listaContactosVcard = new LinkedList<Contacto>();
        listaContactosVcard = lectura.lectura(ruta);

        for (int i = 0; i < listaContactosVcard.size(); i++) {
            listaContactosVcard.get(i).setIdContacto("" + i);
        }

        return listaContactosVcard;

    }

    public void escritura(LinkedList<Contacto> contactos) {
        Escritura e = new Escritura();
        e.guardar(contactos);

    }

    public void guardarResultadosEnBase(LinkedList<Contacto> contactos) throws IOException, SQLException, ClassNotFoundException {
        SqlHelperContact2 a = new SqlHelperContact2();
        System.out.println("Ingresando Contactos...");

        a.insertarContacto(contactos.get(3));

    }

}
