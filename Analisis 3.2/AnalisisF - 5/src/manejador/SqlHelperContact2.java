/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manejador;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.io.IOException;
import java.lang.System.Logger.Level;
import java.sql.SQLException;
import java.util.logging.Logger;
import clases.Contacto;

/**
 *
 * @author luisg
 */
public class SqlHelperContact2 {

    private SqlConn conn;

    //constructor para inicializar el objeto con una nueva conexion
    public SqlHelperContact2() throws IOException {
        conn = new SqlConn();
    }

    //metodo para recuperar los datos de la base de datos usando la conexion
    public void imprimeContacto() throws SQLException {
        //Statement st;
        //ResultSet rs;

        try {
            conn.connect();
            //query que hara la consulta
            String query = "SELECT nombre, apellido, correo FROM contactos INNER JOIN correos ON contactos.id_contacto = 1;";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {

                System.out.println(result.getString("nombre"));
                System.out.println(result.getString("apellido"));
                System.out.println(result.getString("correo"));

            }

        } catch (ClassNotFoundException ex) {

        }
    }

    public void imprimeCorreo() throws SQLException {
        //Statement st;
        //ResultSet rs;

        try {
            conn.connect();
            String query = "SELECT * FROM Correo;";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                System.out.println(result.getString("idContacto"));
                System.out.println(result.getString("correosElectronicos"));

            }
        } catch (ClassNotFoundException ex) {

        }
    }

    public void imprimeNum() throws SQLException {
        //Statement st;
        //ResultSet rs;

        try {
            conn.connect();
            String query = "SELECT * FROM numtelefonico;";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                System.out.println(result.getString("idContacto"));

                System.out.println(result.getString("numerosTelefonicos"));

            }
        } catch (ClassNotFoundException ex) {

        }
    }

    public void imprimeDireccion() throws SQLException {
        //Statement st;
        //ResultSet rs;

        try {
            conn.connect();
            //String query = "SELECT * FROM direccion;";
            String query = "SELECT * FROM direccion;";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                System.out.println(result.getString("idContacto"));
                System.out.println(result.getString("ciudad"));
                System.out.println(result.getString("estado"));
                System.out.println(result.getString("pais"));
            }
        } catch (ClassNotFoundException ex) {

        }
    }

    //INSERTAR
    public void insertarContacto(Contacto a) throws SQLException, ClassNotFoundException {

        try {
            conn.connect();
            String query = "INSERT INTO contactos VALUES (?,?,?,?)";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);

            statement.setString(1, a.getIdContacto());
            statement.setString(2, a.getNombre());
            statement.setString(3, a.getApellido());
            statement.setString(4, a.getAlias());
            //statement.setString(5, a.getImagen());

            //FOR PARA INSERTAR LOS CORREOS
            /* for (int i = 0; i < a.getCorreos().getListaCorreos().size(); i++) {
                String query2 = "INSERT INTO correo VALUES (?,?)";
                PreparedStatement statement2 = (PreparedStatement) conn.getCoon().prepareStatement(query2);
                statement2.setString(1, a.getIdContacto());
                statement2.setString(2, a.getCorreos().getListaCorreos().get(i));
                
            }
            //FOR PARA INSERTAR LOS NUMEROS
           /* for (int j=0; j<a.getNumeros().getListaNumeros().size();j++){
                String query3 = "INSERT INTO numtelefonico (?,?)";
                PreparedStatement statement3 = (PreparedStatement) conn.getCoon().prepareStatement(query3);
                statement3.setString(1, a.getIdContacto());
                statement3.setString(2, a.getNumeros().getListaNumeros().get(j));
            }
            //FOR PARA INSERTAR LA DIRECCIÓN
            String query4 = "INSERT INTO direccion VALUES(?,?,?,?)";
            PreparedStatement statement4 = (PreparedStatement) conn.getCoon().prepareStatement(query4);
            statement4.setString(1, a.getIdContacto());
            statement.setString(2, a.getDireccion().getCalle());
            statement4.setString(3, a.getDireccion().getCiudad());
            statement4.setString(4, a.getDireccion().getEstado());
            statement4.setString(5, a.getDireccion().getPais());*/
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }

        //mandos llamar al metodo para agregar telefonos
        this.insertarTelefonos(a);
    }

    public void insertarCorreos(Contacto a) throws SQLException, ClassNotFoundException {

        try {
            conn.connect();
            String query = "INSERT INTO correos VALUES (?,?,?)";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);

            if (!a.getCorreos().isEmpty()) {
                for (int i = 0; i < a.getCorreos().size(); i++) {
                    statement.setString(1, a.getCorreos().get(i).getEtiqueta());
                    statement.setString(2, a.getCorreos().get(i).getCorreo());
                    statement.setString(3, a.getIdContacto());
                    statement.executeUpdate();
                }
                    
                  
            }else{
                //pasaa
            }

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }

        this.insertarDireccion(a);
    }

    public void insertarTelefonos(Contacto a) throws SQLException, ClassNotFoundException {

        try {

            //para cada elemento de la lista de numero
            for (int i = 0; i < a.getNumeros().size(); i++) {
                conn.connect();
                String query = "INSERT INTO numtelefonicos VALUES (?,?,?)";
                PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);

                statement.setString(1, a.getNumeros().get(i).getEtiqueta());
                statement.setString(2, a.getNumeros().get(i).getNumero());
                statement.setString(3, a.getIdContacto());
                statement.executeUpdate();
            }

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }

        this.insertarCorreos(a);
    }

    public void insertarDireccion(Contacto a) throws SQLException, ClassNotFoundException {

        try {
            conn.connect();
            String query = "INSERT INTO direcciones VALUES (?,?,?,?,?,?,?)";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);

            if(!a.getDirecciones().isEmpty()){
                for (int i = 0; i < a.getDirecciones().size(); i++) {
                    statement.setString(1, a.getDirecciones().get(i).getEtiqueta());
                    statement.setString(2, a.getDirecciones().get(i).getCalle());
                    statement.setString(3, a.getDirecciones().get(i).getColonia());
                    statement.setString(4, a.getDirecciones().get(i).getEstado());
                    statement.setString(5, a.getDirecciones().get(i).getCp());
                    statement.setString(6, a.getDirecciones().get(i).getPais());
                    statement.setString(7, a.getIdContacto());
                    statement.executeUpdate();
                }   
            }
            

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }
        this.insertarFoto(a);
    }

//ACTUALIZAR 
    public void updateContacto() throws SQLException, ClassNotFoundException {
        Contacto contacto = null;
        boolean ejecutar;
        try {
            conn.connect();
            String query = "UPDATE Contacto SET nombre=?,apellido=?, alias=?,imagen=? where idContacto=?";

            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);

            //statement.setString(1, a.getIdContacto());
            statement.setString(1, "Maria");
            statement.setString(2, "Sanchez");
            statement.setString(3, "Mary");
            statement.setString(4, "523691");
            statement.setString(5, "1");

            statement.executeUpdate();

            if (statement.executeUpdate() > 0) {

                System.out.println("\"Los datos han sido modificados con éxito\", \"Operación Exitosa\"");

            } else {
                System.out.println("\"No se ha podido realizar la actualización de los datos\\n\"\n"
                        + "                                          + \"Inténtelo nuevamente.\", \"Error en la operación\"");

            }

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }
    }

    public void updateCorreo() throws SQLException, ClassNotFoundException {
        Contacto contacto = null;
        boolean ejecutar;
        try {
            conn.connect();
            String query = "UPDATE Correo SET correosElectronicos=? where Contacto_idContacto=?";

            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);

            //statement.setString(1, a.getIdContacto());
            statement.setString(1, "ximg1416@gmail.com");
            statement.setString(2, "2");

            statement.executeUpdate();

            if (statement.executeUpdate() > 0) {

                System.out.println("\"Los datos han sido modificados con éxito\", \"Operación Exitosa\"");

            } else {
                System.out.println("\"No se ha podido realizar la actualización de los datos\\n\"\n"
                        + "                                          + \"Inténtelo nuevamente.\", \"Error en la operación\"");

            }

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }
    }

    public void updateNumero() throws SQLException, ClassNotFoundException {
        Contacto contacto = null;
        boolean ejecutar;
        try {
            conn.connect();
            String query = "UPDATE numtelefonico SET numerosTelefonicos=? where Contacto_idContacto=?";

            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);

            //statement.setString(1, a.getIdContacto());
            statement.setString(1, "55639214");
            statement.setString(2, "2");

            statement.executeUpdate();

            if (statement.executeUpdate() > 0) {

                System.out.println("\"Los datos han sido modificados con éxito\"");

            } else {
                System.out.println("\"No se ha podido realizar la actualización de los datos\\n\"\n"
                        + "                                          + \"Inténtelo nuevamente.\", \"Error en la operación\"");

            }

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }
    }

    public void updateDireccion() throws SQLException, ClassNotFoundException {
        Contacto contacto = null;
        boolean ejecutar;
        try {
            conn.connect();
            String query = "UPDATE direccion SET ciudad=?,estado=?,pais=? where Contacto_idContacto=?";

            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);

            //statement.setString(1, a.getIdContacto());
            statement.setString(1, "Monterrey");
            statement.setString(2, "Nuevo Leon");
            statement.setString(3, "Mexico");
            statement.setString(4, "2");

            statement.executeUpdate();

            if (statement.executeUpdate() > 0) {

                System.out.println("\"Los datos han sido modificados con éxito\", \"Operación Exitosa\"");

            } else {
                System.out.println("\"No se ha podido realizar la actualización de los datos\\n\"\n"
                        + "                                          + \"Inténtelo nuevamente.\", \"Error en la operación\"");

            }

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }
    }

//BORRAR
    public void deleteContacto() throws SQLException, ClassNotFoundException {
        try {
            conn.connect();
            String query = "DELETE FROM Contacto WHERE idContacto = ?";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);
            //statement.setString(1, a.getIdContacto());

            statement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }

    }

    public void deleteCorreo() throws SQLException, ClassNotFoundException {
        try {
            conn.connect();
            String query = "DELETE FROM Correo WHERE correosElectronicos = ?";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);
            statement.setString(1, "ximg181@gmail.com");

            statement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }

    }

    public void deleteNumero() throws SQLException, ClassNotFoundException {
        try {
            conn.connect();
            String query = "DELETE FROM numtelefonico WHERE NumerosTelefonicos = ?";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);
            statement.setString(1, "559603847");

            statement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }

    }

    public void deleteDireccion() throws SQLException, ClassNotFoundException {
        try {
            conn.connect();
            String query = "DELETE FROM direccion WHERE Contacto_idContacto = ?";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);
            statement.setString(1, "2");

            statement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }

    }

    private void insertarFoto(Contacto a) throws SQLException, ClassNotFoundException {
        try {
            conn.connect();
            String query = "INSERT INTO fotos VALUES (?,?,?)";
            PreparedStatement statement = (PreparedStatement) conn.getCoon().prepareStatement(query);

            statement.setString(1, a.getFoto().getEqtiqueta());
            statement.setString(2, a.getFoto().getCadena());
            statement.setString(3, a.getIdContacto());
            statement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(SqlHelperContact2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } finally {
            conn.disonnect();
        }
    }

}
