package controladorGui;

import analisisf.ControladorAnalisis;
import clases.Contacto;
import clases.Correo;
import clases.Direccion;
import clases.Foto;
import clases.NumTelefonico;
import escritura.Escritura;
import frames.FrmMenuPrincipal1;
import frames.Inicio;
import frames.Ventana;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Scanner;
import lectura.Lectura;

import manejador.SqlHelperContact2;
import operaciones.GestorContactos;
import operaciones.Operaciones;

/**
 *
 * @author luisg
 */
public class Gui {

    public static String ruta;
    
    public static String Eleccion;
    public static String cadena;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {

        
        //Primero se crea una ventana del frame principal
        Inicio inicio = new Inicio();
        //mostramos la ventana
        inicio.setVisible(true);

        while (ruta == null) {
            //continua esperando
            System.out.print("");
            //continue;
        }
        System.out.println("salio");

        //recuperamos la variable
        System.out.println(ruta);
        ControladorAnalisis analisis = new ControladorAnalisis();
        LinkedList<Contacto> listaContactosVcard;
        listaContactosVcard = new LinkedList<Contacto>();
        //obtenemos la lista de contactos del parseo
        listaContactosVcard = analisis.lectura(ruta);

        //Lista de contactos a exportar.
        LinkedList<Contacto> ListaContactosE = new LinkedList<>();
        //Lista Contactos IncompletosCompletos
        LinkedList<Contacto> ListaContactosIncompletos = new LinkedList<>();

        //Lista Contactos Completos
        LinkedList<Contacto> ListaContactosCompletos = new LinkedList<>();

        //Lista Contactos SinImagen
        LinkedList<Contacto> ListaContactosSinImagen = new LinkedList<>();

        //Lista Contactos Repetidos
        LinkedList<Contacto> ListaContactosRepetidos = new LinkedList<>();

        System.out.println("-------------------------------------------------");

        Operaciones op = new Operaciones();

        //copiar la lista de contactos para realizar gestion. 
        op.CopiarListas(listaContactosVcard, ListaContactosE);
        System.out.println("Cantidad de contactos a Gestionar ");
        System.out.println(ListaContactosE.size());
        System.out.println("Muestra los contactos a Gestionar.  ");
        System.out.println(ListaContactosE);

        //Metodo que guarda en otra lista los contactos con campos faltantes y los completos 
        op.ObtenerCompletosEincompletos(listaContactosVcard, ListaContactosIncompletos, ListaContactosCompletos);

        System.out.println("-------------------------------------------------");

        GestorContactos gestor = new GestorContactos();
        gestor.ListaParseo(ruta);
        gestor.ContactosCompletos();
        System.out.println("-------------------------------------------------");

        System.out.println(gestor.getListaContactosCompletos().size());
        System.out.println(gestor.getListaContactosCompletos());

        System.out.println("-------------------------------------------------");

        String Opcion = "";
        Scanner entrada = new Scanner(System.in);

        //creamos una nueva ventana
        do {

            // Operaciones opcion = new Operaciones();
            System.out.println("\nIngrese el número de cada opción para ejecutar una orden.\n");
            cadena += "\nIngrese el número de cada opción para ejecutar una orden.\n";
            System.out.println("1.Mostrar Contactos  Completos.");
            cadena += "\n1.Mostrar Contactos  Completos.\n";
            System.out.println("2.Mostrar Contactos  Incompletos.");
            cadena += "\n2.Mostrar Contactos  Incompletos.\n";
            System.out.println("3.Mostrar Contactos  Sin imagen.");
            cadena += "\n3.Mostrar Contactos  Sin imagen.\n";
            System.out.println("4.Mostrar Contactos  Duplicados.");
            cadena += "\n4.Mostrar Contactos  Duplicados.\n";
            System.out.println("5.Buscar contacto por ID.");
            cadena += "\n5.Buscar contacto por ID.\n";
            System.out.println("6.Borrar Contacto.");
            cadena += "\n6.Borrar Contacto.\n";
            System.out.println("7.Mostrar Contactos Actuales.");
            cadena += "\n7.Mostrar Contactos Actuales.\n";
            System.out.println("8.Completar Contacto.");
            cadena += "\n8.Completar Contacto.\n";
            System.out.println("9.Salir.");
            cadena += "\n9.Salir.\n";

            Ventana.caja.setText(cadena);

            while (Eleccion == null) {
                System.out.print("");
            }

            Ventana.caja.setText("");
            String mandar = "";
            
        switch (Eleccion) {
                case "1":
                    cadena = "";
                    Ventana.caja.setText(cadena);
                    System.out.println("Contactos Completos: ");
                    cadena += "Contactos Completos:\n";
                    System.out.println(ListaContactosCompletos.size());
                    cadena += ListaContactosCompletos.size() + "\n";
                    System.out.println(ListaContactosCompletos);
                    cadena += ListaContactosCompletos.toString() + "\n";
                    System.out.println("\n");
                    Ventana.caja.setText(cadena);
                    Eleccion = null;
                    break;
                case "2":
                    cadena = "";
                    Ventana.caja.setText(cadena);
                    System.out.println("Contactos Incompletos:");
                    cadena += "Contactos Incompletos:\n";
                    System.out.println(ListaContactosIncompletos.size());
                    cadena += ListaContactosIncompletos.size()+"\n";
                    System.out.println(ListaContactosIncompletos);
                    cadena += ListaContactosIncompletos.toString()+"\n";
                    System.out.println("\n");
                    cadena += "\n";
                    Ventana.caja.setText(cadena);
                    Eleccion = null;
                    break;
                case "3":
                    cadena = "";
                    Ventana.caja.setText(cadena);
                    System.out.println("Contactos Sin Imagen:");
                    cadena += "Contactos Sin Imagen:\n";
                    op.ObtenerSinImagen(ListaContactosE, ListaContactosSinImagen);
                    
                    System.out.println(ListaContactosSinImagen.size());
                    cadena += ListaContactosSinImagen.size()+"\n";
                    System.out.println(ListaContactosSinImagen);
                    cadena +=ListaContactosSinImagen.toString()+"\n";
                    System.out.println("\n");
                    cadena += "\n";
                    Ventana.caja.setText(cadena);
                    Eleccion = null;
                    break;
                case "4":
                    cadena = "";
                    Ventana.caja.setText(cadena);
                    System.out.println("Contactos Duplicados:");
                    cadena += "Contactos Duplicados:\n";
                    op.BuscarRepetidos(ListaContactosE, ListaContactosRepetidos);
                    System.out.println(ListaContactosRepetidos.size());
                    cadena += ListaContactosRepetidos.size() +"\n";
                    System.out.println(ListaContactosRepetidos);
                    cadena += ListaContactosRepetidos +"\n";
                    System.out.println("\n");
                    cadena += "\n";
                    Ventana.caja.setText(cadena);
                    Eleccion = null;
                    break;
                case "5":
                    cadena = "";
                    Ventana.caja.setText(cadena);
                    System.out.println("Ingrese ID:");
                    cadena += "Ingrese ID:\n";
                    Ventana.respuesta.setText("");
                    Ventana.caja.setText(cadena);
                    Eleccion = null;
                    while(Eleccion == null){
                       System.out.println(""); 
                    }
                    
                    op.buscaId(ListaContactosE, Eleccion);
                     
                    System.out.println("\n");
                    cadena += "\n";
                    Ventana.caja.setText(cadena);
                    Eleccion = null;
                    break;
                case "6":
                    cadena = "";
                    Ventana.caja.setText(cadena);
                    System.out.println("Ingrese ID:");
                    System.out.println("Ingrese el Id del contacto a borrar:");
                    cadena += "Ingrese el Id del contacto a borrar:\n";
                    Ventana.respuesta.setText("");
                    Ventana.caja.setText(cadena);
                    Eleccion = null;
                    while(Eleccion == null){
                       System.out.println(""); 
                    }
                    //int idkey = Integer.parseInt(identrada);
                    op.BorrarContacto(ListaContactosE, Eleccion);
                    System.out.println("\n");
                    cadena += "\n";
                    Ventana.caja.setText(cadena);
                    Eleccion = null;
                    break;
                case "7":
                    cadena = "";
                    Ventana.caja.setText(cadena);
                    System.out.println("Contactos Actuales :");
                    cadena += "Ingrese el Id del contacto a borrar:\n";
                    System.out.println(ListaContactosE.size());
                    cadena += ListaContactosE.size()+"\n";
                    System.out.println(ListaContactosE);
                    cadena += ListaContactosE.toString()+"\n";
                    System.out.println("\n");
                    cadena += "\n";
                    Ventana.caja.setText(cadena);
                    //analisis.guardarResultadosEnBase(ListaContactosE);
                    Eleccion = null;
                    break;
                case "8":
                    System.out.println("Completar contacto:");
                    System.out.println("Ingrese el Id del contacto a Completar:");
                    String key = entrada.next();
                    op.CompletarContacto(ListaContactosE, key);
                   
                    System.out.println("\n");
                    break;
                case "9":
                    Opcion = "9";
                    break;
                default:
                    break;
            }

        } while (!"9".equals(Opcion));
        

    }

}
