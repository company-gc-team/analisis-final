/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operaciones;

import clases.Contacto;
import clases.NumTelefonico;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import lectura.Lectura;

/**
 *
 * @author hp
 */
public class GestorContactos {
     
        LinkedList<Contacto> listaContactosVcard = new LinkedList<Contacto>();
        //Lista de contactos a exportar.
        LinkedList <Contacto> ListaContactosE = new LinkedList <> ();
        //Lista Contactos IncompletosCompletos
        LinkedList<Contacto> ListaContactosIncompletos = new LinkedList<>();

        //Lista Contactos Completos
        LinkedList<Contacto> ListaContactosCompletos = new LinkedList<>();

        //Lista Contactos SinImagen
        LinkedList<Contacto> ListaContactosSinImagen = new LinkedList<>();

        //Lista Contactos Repetidos
        LinkedList<Contacto> ListaContactosRepetidos = new LinkedList<>();
        
        Operaciones oper = new Operaciones();
        
       
       
    
   
      public LinkedList  <Contacto> ListaParseo(String ruta) throws IOException {
   
        Lectura lectura = new Lectura();
        Operaciones ope = new Operaciones();
        listaContactosVcard = lectura.lectura(ruta);
        
        for (int i = 0; i < listaContactosVcard.size(); i++) {
            listaContactosVcard.get(i).setIdContacto("" + i);
        }
         //Metodo que guarda en otra lista los contactos con campos faltantes y los completos 
        ope.CopiarListas(listaContactosVcard,ListaContactosE);
        return ListaContactosE;
      
    }

    public void ContactosCompletos() {
        Operaciones op = new Operaciones();
        op.ObteneCompletos(ListaContactosE,ListaContactosCompletos);
        //return ListaContactosCompletos;
    }

    public LinkedList<Contacto> getListaContactosE() {
        return ListaContactosE;
    }

    public LinkedList<Contacto> getListaContactosCompletos() {
        return ListaContactosCompletos;
    }
    
    
  
    
     
     
     
     
     
    
}
