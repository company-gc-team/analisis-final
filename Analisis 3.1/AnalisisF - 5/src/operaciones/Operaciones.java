package operaciones;

import clases.Contacto;
import clases.Correo;
import clases.Direccion;
import clases.NumTelefonico;
import controladorGui.Gui;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Sergio Castillo 2182000613
 */
public class Operaciones {

    public LinkedList insertarContacto(LinkedList listaContactos, Contacto contac) {

        listaContactos.add(contac);
        return listaContactos;

    }

    public Contacto regresarContacto(LinkedList listaContacto, int pos) {

        Contacto cont = (Contacto) listaContacto.get(pos);
        return cont;

    }

    public int buscarContacto(LinkedList listaContactos, String key) {

        int pos = -1;

        for (int i = 0; i < listaContactos.size(); ++i) {

            Contacto a = (Contacto) listaContactos.get(i);

            if (a.getIdContacto().compareTo(key) == 0) {
                pos = i;
                break;
            }

        }
        return pos;
    }

    public LinkedList buscaId(LinkedList<Contacto> lista, String id) {

        int pos = buscarContacto(lista, id);

        if (pos == -1) {

            System.out.println("No existe en la lista " + id);
            Gui.cadena = "No existe en la lista \n";
        } else {

            System.out.println("Los datos del Contacto son: " + lista.get(pos));
            Gui.cadena = "Los datos del Contacto son:"+lista.get(pos).toString()+"\n";
        }

        return lista;
    }

    public LinkedList CopiarListas(LinkedList lista1, LinkedList lista2) {

        for (int i = 0; i < lista1.size(); ++i) {

            lista2.add(lista1.get(i));

        }
        return lista2;
    }

    public LinkedList BorrarContacto(LinkedList listaContactos, String key) {

        int posBorrar = buscarContacto(listaContactos, key);

        if (posBorrar == -1) {

            System.out.println("No existe en la lista " + key);

        } else {

            listaContactos.remove(posBorrar);
            System.out.println("Los datos del Contacto se han eliminado.");
        }

        return listaContactos;

    }


    /*public void buscafaltante(LinkedList<Contacto> lista) {

        for (int i = 0; i < lista.size(); ++i) {

            Contacto a = (Contacto) lista.get(i);

            if (lista.get(i).getApellido() == null || a.getApellido().compareTo("") == 0) {
                System.out.println("falta apellido del contacto: " + a.getIdContacto());
                System.out.println("Ingrese Apellido:");
                Scanner entrada = new Scanner(System.in);
                String campoApellido = entrada.next();
                a.setApellido(campoApellido);
                //int pos;
                //pos=(int) lista.get(i);
                //lista.add(i, entrada);
                System.out.println("Campo ingresado.");
            }
            if (lista.get(i).getNombre() == null || a.getNombre().compareTo("") == 0) {
                System.out.println("falta nombre del contacto: " + a.getIdContacto());
                System.out.println("Ingrese Nombre:");
                Scanner entrada2 = new Scanner(System.in);
                String campoNombre = entrada2.next();
                a.setNombre(campoNombre);
                System.out.println("nombre ingresado.");
            }
            if (lista.get(i).getApellido() == null || a.getAlias().compareTo("") == 0) {
                System.out.println("falta Alias del contacto: " + a.getIdContacto());
                System.out.println("Ingrese Alias:");
                Scanner entrada3 = new Scanner(System.in);
                String campoNombre = entrada3.next();
                a.setNombre(campoNombre);
                System.out.println("Alias ingresado ingresado.");
            }

            // Recorre la lista de Correos para agregar correos
               if(a.getCorreos().isEmpty()) {
                System.out.println("Entraaaaa+++++");
                do{
                    
                    String eleccion =  "";
                     switch (eleccion) {
                     case "1":
                }
                
                
               for (int k = 0; k < a.getCorreos().size(); ++k) {
                    //Correo n=(Correo)a.getCorreos().get(k);
                    //f(n.getCorreo()== null || a.getAlias().compareTo("") == 0){
                       
                System.out.println("Holaaaaaaaaaaa");
                System.out.println("Ingrese Correo:");
                Scanner entrada4 = new Scanner(System.in);
                String campoCorreo = entrada4.next();
                LinkedList<Correo> correosC1 = new LinkedList <>();
                Correo correo = new Correo();
                
                System.out.println("Correo ingresado.");
               }  
                
                
                    //Correo n=(Correo)a.getCorreos().get(k);
                    //if(a.getCorreos().isEmpty()){
                    //System.out.println("Entro++++++++");
                   // System.out.println("faltan Correos, del contacto:  " + a.getCorreos());
                    //System.out.println("Ingrese Correos al contacto:");

                    //Scanner entrada = new Scanner(System.in);
                    //String campoCorreos = entrada.next();
                    //lista.get(i).getNumeros().get(k).setNumero(campoCorreos);
                    //a.getNumeros().get(i).setNumero(campoCorreos);
                    //System.out.println("Correo ingresado ingresado ingresado.");

                }
            }

       
    
    //}
     */
    public void CompletarContacto(LinkedList<Contacto> lista, String key) {

        String numCadena = key;
        
        int pos = Integer.parseInt(numCadena);

        if(pos > lista.size()){
          
        System.out.println("La lista no contiene el id ingresado:" + pos);
      
        }
        Contacto a = (Contacto) lista.get(pos);

        String Opcion = "";
        Scanner entrada = new Scanner(System.in);

        do {

            System.out.println("Se Completaran campos faltantes del contacto:" + key);
            System.out.println("\nIngrese el número de cada opción para ejecutar una orden.\n");
            System.out.println("1.Agregar o cambiar Apellido.");
            System.out.println("2.Agregar o cambiar Nombre.");
            System.out.println("3.Agregar o cambiar Alias.");
            System.out.println("4.Agregar o editar  numeros telefonicos.");
            System.out.println("5.Agregar correos electronicos.");
            System.out.println("6.Agregar direccion.");
            System.out.println("7.Salir.");

            String Eleccion = entrada.next();

            switch (Eleccion) {
                case "1":
                    System.out.println("Apellido Actual: " + a.getApellido());
                    System.out.println("Ingrese Apellido: ");
                    String ap = entrada.next();
                    a.setApellido(ap);
                    System.out.println("Apellido Ingresado: " + a.getApellido());
                    System.out.println("\n");
                    break;
                case "2":
                    System.out.println("Nombre Actual: " + a.getNombre());
                    System.out.println("Ingrese Nombre: ");
                    String nom = entrada.next();
                    a.setNombre(nom);
                    System.out.println("Nombre Ingresado: " + a.getNombre());
                    System.out.println("\n");
                    break;
                case "3":
                    System.out.println("Alias Actual: " + a.getAlias());
                    System.out.println("Ingrese Alias: ");
                    String al = entrada.next();
                    a.setAlias(al);
                    System.out.println("Alias Ingresado: " + a.getAlias());
                    System.out.println("\n");
                    break;

                case "4":

                    System.out.println("Numero Actuales:");

                    if (!a.getNumeros().isEmpty()) {
                        System.out.println(a.getNumeros());
                    }

                    if (a.getNumeros().isEmpty()) {
                        System.out.println("No hay registros.");
                    }

                    boolean salir = false;

                    String opcion;

                    do {

                        System.out.println("¿Desea agregar numero?");
                        System.out.println("1. Si");
                        System.out.println("2. No , Salir");

                        System.out.println("Escribe una de las opciones");
                        opcion = entrada.next();

                        switch (opcion) {
                            case "1":
                                int posln = 0;
                                System.out.println("Ingrese tipo de numero:"
                                        + "1:Personal , 2: trabajo ,  3: Casa\n");

                                String etiq = entrada.next();

                                if (etiq.compareTo("1") == 0) {

                                    a.getNumeros().get(posln).setEtiqueta("TEL;CELL");
                                    System.out.println("Tipo ingresado.");
                                }

                                if (etiq.compareTo("2") == 0) {
                                    a.getNumeros().get(posln).setEtiqueta("TEL;WORK");
                                    System.out.println("Tipo ingresado.");
                                }

                                System.out.println("Ingrese Numero:");

                                String numero = entrada.next();
                                a.getNumeros().get(posln).setNumero(numero);
                                System.out.println("numero ingresado.");
                                posln = +1;
                                break;

                            case "2":
                                salir = true;
                                break;
                        }
                    } while (!salir);

                    break;

                case "5":
                    System.out.println("Correos Actuales:");

                    if (!a.getCorreos().isEmpty()) {
                        System.out.println(a.getCorreos());
                    }

                    if (a.getCorreos().isEmpty()) {
                        System.out.println("No hay registros.");
                      
                    }

                    boolean salirc = false;

                    String opc;

                    do {

                        System.out.println("¿Desea agregar  correos?");
                        System.out.println("1. Agregar");
                        System.out.println("2. Cambiar");
                        System.out.println("3. No , Salir");

                        System.out.println("Escribe una de las opciones");
                        opc = entrada.next();

                        switch (opc) {
                            case "1":
                                int poslc = 0;
                                System.out.println("Ingrese tipo de correo"
                                        + "1:Personal , 2: trabajo \n");

                                String etiqc = entrada.next();

                                if (etiqc.compareTo("1") == 0) {
                                    Correo corr = new Correo();
                                    corr.setEtiqueta("EMAIL;HOME");
                                    System.out.println("Tipo ingresado.");
                                    //corr.setCorreo("ejemplo");
                                    System.out.println("Ingrese Correo:");
                                    String email = entrada.next();
                                    corr.setCorreo(email);
                                    System.out.println("Correo ingresado.");
                                    a.getCorreos().add(corr);
                                    
                                 
                                }

                                if (etiqc.compareTo("2") == 0) {
                                    Correo corr = new Correo();
                                    corr.setEtiqueta("EMAIL;WORK");
                                    System.out.println("Tipo ingresado.");
                                    //corr.setCorreo("ejemplo");
                                    System.out.println("Ingrese Correo:");
                                    String email = entrada.next();
                                    corr.setCorreo(email);
                                    System.out.println("Correo ingresado.");
                                    a.getCorreos().add(corr);
                                }
                                poslc = +1;
                                break;
                            case "2":
                                System.out.println("Correos actuales ");
                                System.out.println(a.getCorreos());

                                
                                
                                break;
                            case "3":
                                salirc = true;
                                break;
                        }
                    } while (!salirc);
                    break;
                    
                case "6":
                   System.out.println("Datos de Direccion :");

                    if (!a.getDirecciones().isEmpty()) {
                        System.out.println(a.getDirecciones());
                    }

                    if (a.getDirecciones().isEmpty()) {
                        System.out.println("No hay registros.");
                      
                    }

                    boolean salird = false;

                    String opcd;

                    do {

                        System.out.println("¿Desea agregar Direccion?");
                        System.out.println("1. Agregar");
                        System.out.println("2. Cambiar");
                        System.out.println("3. No , Salir");

                        System.out.println("Escribe una de las opciones");
                        opcd = entrada.next();

                        switch (opcd) {
                            case "1":
                                int posld = 0;
                                System.out.println("Ingrese tipo de direccion"
                                        + "1:Personal ");

                                String etiqd = entrada.next();

                                if (etiqd.compareTo("1") == 0) {
                                    Direccion dirr = new Direccion();
                                    dirr.setEtiqueta("ADR;HOME");
                                    System.out.println("Tipo ingresado.");
                                    //corr.setCorreo("ejemplo");
                                    System.out.println("Para saltar o dejar campo en vacio"
                                            + "ingrese 00");
                                    System.out.println("Ingrese calle:");
                                    String direc = entrada.next();
                                    if(direc.compareTo("00")==0){
                                    dirr.setCalle(direc); 
                                    System.out.println("Calle ingresada como vacia.");
                                    }
                                    else {
                                    dirr.setCalle(direc);
                                    System.out.println("Calle ingresada.");
                                    }
                                    
                                                      
                                    System.out.println("Ingrese colonia:");
                                    direc = entrada.next();
                                    if(direc.compareTo("00")==0){
                                    dirr.setColonia(direc);
                                    System.out.println("Colonia ingresada como vacia.");
                                    }
              
                                    else {
                                     dirr.setColonia(direc);
                                    System.out.println("Calle ingresada.");       
                                    }
                                    
                              
                                    System.out.println("Ingrese Estado:");
                                    direc = entrada.next();
                                    
                                    if(direc.compareTo("00")==0){
                                    dirr.setEstado(direc);
                                    System.out.println("Estado vacio."); 
                                    }
                                    else{
                                    dirr.setEstado(direc);
                                    System.out.println("Estado ingresado.");  
                                    }
                                    
                                    
                                    System.out.println("Ingrese Cp:");
                                    direc = entrada.next();
                                    if(direc.compareTo("00")==0){
                                    dirr.setCp(direc);
                                    System.out.println("Cp ingresado vacio.");
                                    }
                                    else{
                                    dirr.setCp(direc);
                                    System.out.println("Cp ingresado.");     
                                    }
                                    
                                    
                                    System.out.println("Ingrese pais:");
                                    direc = entrada.next();
                                    if(direc.compareTo("00")==0){
                                    dirr.setPais(direc);
                                    System.out.println("Pais ingresado vacio.");
                                    }
                                    else {
                                     dirr.setPais(direc);
                                    System.out.println("Pais ingresado.");
                                    }
                                    a.getDirecciones().add(dirr);                                
                                }

                                posld = +1;
                                break;
                            case "2":
                                System.out.println("Direccion actual ");
                                System.out.println(a.getDirecciones());

                                break;
                            case "3":
                                salird = true;
                                break;
                        }
                    } while (!salird);
                    break;
                    
       
                case "7":
                    Opcion = "7";
                    break;
                default:
                    break;
            }

        } while (!"7".equals(Opcion));

    }

    // Metodos para clasificar Contactos
    public LinkedList BuscarRepetidos(LinkedList<Contacto> lista1, LinkedList lista2) {
        int flag = 0;
        int flag1 = 0;
        for (int i = 0; i < lista1.size(); i++) {
            //Contacto a = (Contacto) lista1.get(i);
            for (int j = 0; j < lista1.size(); j++) {
                //Contacto b = (Contacto) lista1.get(j);
                if (lista1.get(i).getIdContacto() == lista1.get(j).getIdContacto()
                        || !lista1.get(i).getAlias().equals(lista1.get(j).getAlias())) {
                } else {
                    System.out.println(" coinciden en Alias");
                    Gui.cadena += "coinciden en Alias\n";
                    //System.out.println("Id: " + lista1.get(j).getIdContacto());
                    flag = 1;
                }
                if (!lista1.get(i).getNumeros().isEmpty() && !lista1.get(j).getNumeros().isEmpty()) {
                    
                    System.out.println(" coinciden en lista no vacia");
                    Gui.cadena += "coinciden en lista no vacia\n";
                    for (int k = 0; k < lista1.get(i).getNumeros().size(); k++) {
                        for (int l = 0; l < lista1.get(j).getNumeros().size(); l++) {
                            //if (lista1.get(i).getNumeros().get(k).getNumero() == lista1.get(j).getNumeros().get(l).getNumero()
                                //) 
                            if (lista1.get(i).getIdContacto() == lista1.get(j).getIdContacto()
                        || !lista1.get(i).getNumeros().get(k).getNumero().equals(lista1.get(j).getNumeros().get(l).getNumero())){
                                System.out.println("coinciden en los numeros");
                                Gui.cadena += "coinciden en los numeros\n";
                                System.out.println("Id: " + lista1.get(j).getNumeros().get(l).getNumero());
                                Gui.cadena +="Id: " + lista1.get(j).getNumeros().get(l).getNumero()+"\n";
                                flag1 = 1;
                            }

                        }

                    }

                } /*else {

                    for (int k = 0; k < lista1.get(i).getNumeros().size(); k++) {
                        for (int l = 0; l < lista1.get(j).getNumeros().size(); l++) {
                            if (lista1.get(i).getNumeros().get(k).getNumero() == lista1.get(j).getNumeros().get(l).getNumero()) {
                                System.out.println(" coinciden -----");
                                System.out.println("Id: " + lista1.get(j).getNumeros().get(l).getNumero());
                                flag1 = 1;
                            }

                        }

                    }*/

                }

            //}

        if (flag == 1) {
                System.out.println("Entro -----------");
                lista2.add(lista1.get(i));
                //System.out.println("Exiten Reptidos");
                //System.out.println("Flag=" + flag);
            }
            //System.out.println("Flag=" + flag);
            flag = 0;    
        }

        return lista2;
    }

    public LinkedList ObtenerCompletosEincompletos(LinkedList<Contacto> lista1, LinkedList lista2, LinkedList lista3) {
        int flag = 0;
        for (int i = 0; i < lista1.size(); ++i) {

            Contacto a = (Contacto) lista1.get(i);

            if (a.getApellido() == null) {
                flag = 1;
            }

            if (a.getNombre() == null) {

                flag = 1;

            }

            if (a.getAlias() == null) {
                flag = 1;
            }

            // recorre la lista de numeros en busca de vacios
            for (int j = 0; j < a.getNumeros().size(); ++j) {
                NumTelefonico n = (NumTelefonico) a.getNumeros().get(j);
                if (n.getNumero() == null) {
                    flag = 1;
                }

                if (n.getEtiqueta() == null) {
                    flag = 1;
                }
            }

            // recorre la lista de correos en busca de vacios
            for (int k = 0; k < a.getCorreos().size(); ++k) {
                Correo n = (Correo) a.getCorreos().get(k);
                if (n.getCorreo() == null) {

                    flag = 1;

                }

                if (n.getEtiqueta() == null) {

                    flag = 1;

                }

            }

            // recorre la lista de direccion en busca de vacios
            for (int l = 0; l < a.getDirecciones().size(); ++l) {
                Direccion n = (Direccion) a.getDirecciones().get(l);

                if (n.getEtiqueta() == null) {

                    flag = 1;

                }

                if (n.getCalle() == null) {

                    flag = 1;

                }

                if (n.getColonia() == null) {

                    flag = 1;

                }

                if (n.getEstado() == null) {

                    flag = 1;

                }

                if (n.getCp() == null) {

                    flag = 1;

                }

                if (n.getPais() == null) {

                    flag = 1;
                    //System.out.println("bandera:" + flag );
                }

            }

            if (a.getFoto() == null) {

                flag = 1;

            }

            //if(a.getFoto().getCadena()== null){ 
            //flag=1; 
            //}
            if (flag != 0) {
                lista1.get(i).setCompleto(false);
                lista2.add(lista1.get(i));

                //a.setCompleto(false);
            }

            if (flag == 0) {
                lista1.get(i).setCompleto(true);
                lista3.add(lista1.get(i));
                //a.setCompleto(true);
            }

            flag = 0;

        }

        return lista2;
    }

    public LinkedList ObtenerSinImagen(LinkedList<Contacto> lista1, LinkedList lista2) {
        int flag = 0;
        for (int i = 0; i < lista1.size(); ++i) {

            Contacto a = (Contacto) lista1.get(i);

            if (a.getFoto() == null) {

                flag = 1;

            }

            //if("vacio".equals(a.getFoto().getCadena())){ 
            //flag=1; 
            // }
            if (flag == 1) {
                lista2.add(lista1.get(i));
            }

            flag = 0;

        }

        return lista2;
    }

    public LinkedList Duplicate(LinkedList lista1, LinkedList lista2) {

        LinkedHashSet<Contacto> hashSet = new LinkedHashSet<>(lista1);
        LinkedList<Contacto> lista3 = new LinkedList<>(hashSet);
        for (int i = 0; i < lista3.size(); ++i) {
            //System.out.println(lista3);
            lista2.add(lista3.get(i));

        }

        return lista2;

    }

    public LinkedList BuscarRepetidos2(LinkedList<Contacto> lista1, LinkedList lista2) {
        //int flag1=0;
        //int flag2=0;
        for (int i = 0; i < lista1.size(); i++) {
            int flag1 = 0;
            int flag2 = 0;
            //Contacto a=(Contacto)lista1.get(i);
            for (int j = 0; j < lista1.size(); j++) {
                //int flag1=0;
                //Contacto b=(Contacto)lista1.get(j);
                if (i != j && lista1.get(i).getApellido() == lista1.get(j).getApellido()) {
                    //&& a.getNombre()== b.getNombre() && a.getAlias()== b.getAlias() ){ 
                    flag1 = 10;
                    System.out.println("Flag1= aaaaa " + flag1);
                    System.out.println(" coincidennn ");
                }

                //flag1=0;
            }
            //flag1 = 0;

            /* for(int k=0;k<lista1.get(i).getNumeros().size();k++){
            //Contacto b=(Contacto)lista1.get(i);
            //int flag2=0;
           //NumTelefonico n=(NumTelefonico)a.getNumeros().get(k);
            for(int m=0;m<lista1.get(i).getNumeros().size();m++){
           //NumTelefonico n1=n=(NumTelefonico)a.getNumeros().get(m);
             if(k!=m && lista1.get(i).getNumeros().get(k).getEtiqueta()==lista1.get(i).getNumeros().get(m).getEtiqueta()){
                //&& n.getNumero()==n1.getNumero()){ 
                flag2= 15;
                System.out.println("Flag2= AAAA "+ flag2);
                System.out.println(" coinciden ");
            }
             
             //flag2=0;
            }
        }
            //flag2 = 0;
             */
            //if (flag1 ==1  && flag2 == 1 ) {
            // lista2.add(lista1.get(i));
            //System.out.println("Prueba");
            //System.out.println("Flag=" + flag);
            //}
        }
        //System.out.println("Flag=" + flag);
        //flag 

        return lista2;
    }

    public LinkedList BuscarRepetidos3(LinkedList<Contacto> lista1, LinkedList lista2) {
        int flag1 = 0;
        int flag2 = 0;

        for (int i = 0; i < lista1.size(); i++) {

            Contacto a = (Contacto) lista1.get(i);
            for (int j = 0; j < lista1.size(); j++) {
                Contacto b = (Contacto) lista1.get(j);
                if (i != j && a.getApellido() == b.getApellido()
                        && a.getNombre() == b.getNombre()
                        && a.getAlias() == b.getAlias()) {
                    flag1 = 10;
                    System.out.println("Flag= aaa " + flag1);
                    System.out.println(" coinciden ");
                }

                //Recorre Lista de Contactos 
                for (int k = 0; k < lista1.get(i).getNumeros().size(); k++) {

                    for (int m = 0; m < lista1.get(i).getNumeros().size(); m++) {

                        if (k != m && lista1.get(i).getNumeros().get(k).getEtiqueta() == lista1.get(i).getNumeros().get(m).getEtiqueta()) {
                            //&& n.getNumero()==n1.getNumero()){ 
                            flag2 = 15;
                            System.out.println("Flag2= AAA " + flag2);
                            System.out.println(" coinciden ");

                        }

                    }

                }
            }
            System.out.println("suma de flags = " + (flag1 + flag2));
            System.out.println("Flag1= es " + flag1);
            System.out.println("Flag2= es " + flag2);
            if (flag1 == 10 && flag2 == 15) {
                lista2.add(lista1.get(i));
                System.out.println("repetodos hasta numeros");
                //System.out.println("suma de flags = " + (flag1 + flag2)) ;
            }

            flag1 = 0;
            flag2 = 0;
        }

        return lista2;
    }

    public LinkedList ObtenerIncompletos(LinkedList<Contacto> lista1, LinkedList lista2) {
        int flag = 0;
        for (int i = 0; i < lista1.size(); ++i) {

            Contacto a = (Contacto) lista1.get(i);

            if (a.getApellido() == null) {
                flag = 1;
            }

            if (a.getNombre() == null) {

                flag = 1;

            }

            if (a.getAlias() == null) {
                flag = 1;
            }

            // recorre la lista de numeros en busca de vacios
            for (int j = 0; j < a.getNumeros().size(); ++j) {
                NumTelefonico n = (NumTelefonico) a.getNumeros().get(j);
                if (n.getNumero() == null) {
                    flag = 1;
                }

                if (n.getEtiqueta() == null) {
                    flag = 1;
                }
            }

            // recorre la lista de correos en busca de vacios
            for (int k = 0; k < a.getCorreos().size(); ++k) {
                Correo n = (Correo) a.getCorreos().get(k);
                if (n.getCorreo() == null) {

                    flag = 1;

                }

                if (n.getEtiqueta() == null) {

                    flag = 1;

                }

            }

            // recorre la lista de direccion en busca de vacios
            for (int l = 0; l < a.getDirecciones().size(); ++l) {
                Direccion n = (Direccion) a.getDirecciones().get(l);

                if (n.getEtiqueta() == null) {

                    flag = 1;

                }

                if (n.getCalle() == null) {

                    flag = 1;

                }

                if (n.getColonia() == null) {

                    flag = 1;

                }

                if (n.getEstado() == null) {

                    flag = 1;

                }

                if (n.getCp() == null) {

                    flag = 1;

                }

                if (n.getPais() == null) {

                    flag = 1;
                    //System.out.println("bandera:" + flag );
                }

            }

            if (a.getFoto() == null) {

                flag = 1;

            }

            //if(a.getFoto().getCadena()== null){ 
            //flag=1; 
            //}
            if (flag != 0) {
                lista1.get(i).setCompleto(false);
                lista2.add(lista1.get(i));

                //a.setCompleto(false);
            }
            flag = 0;

        }

        return lista2;
    }
    
    
    public LinkedList ObteneCompletos(LinkedList<Contacto> lista1, LinkedList lista2) {
        int flag = 0;
        for (int i = 0; i < lista1.size(); ++i) {

            Contacto a = (Contacto) lista1.get(i);

            if (a.getApellido() == null) {
                flag = 1;
            }

            if (a.getNombre() == null) {

                flag = 1;

            }

            if (a.getAlias() == null) {
                flag = 1;
            }

            // recorre la lista de numeros en busca de vacios
            for (int j = 0; j < a.getNumeros().size(); ++j) {
                NumTelefonico n = (NumTelefonico) a.getNumeros().get(j);
                if (n.getNumero() == null) {
                    flag = 1;
                }

                if (n.getEtiqueta() == null) {
                    flag = 1;
                }
            }

            // recorre la lista de correos en busca de vacios
            for (int k = 0; k < a.getCorreos().size(); ++k) {
                Correo n = (Correo) a.getCorreos().get(k);
                if (n.getCorreo() == null) {

                    flag = 1;

                }

                if (n.getEtiqueta() == null) {

                    flag = 1;

                }

            }

            // recorre la lista de direccion en busca de vacios
            for (int l = 0; l < a.getDirecciones().size(); ++l) {
                Direccion n = (Direccion) a.getDirecciones().get(l);

                if (n.getEtiqueta() == null) {

                    flag = 1;

                }

                if (n.getCalle() == null) {

                    flag = 1;

                }

                if (n.getColonia() == null) {

                    flag = 1;

                }

                if (n.getEstado() == null) {

                    flag = 1;

                }

                if (n.getCp() == null) {

                    flag = 1;

                }

                if (n.getPais() == null) {

                    flag = 1;
                    //System.out.println("bandera:" + flag );
                }

            }

            if (a.getFoto() == null) {

                flag = 1;

            }

            //if(a.getFoto().getCadena()== null){ 
            //flag=1; 
            //}
           if (flag == 0) {
                lista1.get(i).setCompleto(true);
                lista2.add(lista1.get(i));
                //a.setCompleto(true);
            }
            flag = 0;

        }

        return lista2;
    }
}
