package clases;

import java.util.LinkedList;

/**
 * @author hp
 */
public class Direccion {

    @Override
    public String toString() {
        return "Direccion{" + "etiqueta=" + etiqueta + ", calle=" + calle + ", colonia=" + colonia + ", estado=" + estado + ", cp=" + cp + ", pais=" + pais + '}';
    }

    private String etiqueta;
    private String calle;
    private String colonia;
    private String estado;
    private String cp;
    private String pais;

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

}
