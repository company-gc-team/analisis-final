
package clases;

import java.util.LinkedList;

/**
 *
 * @author luisg
 */
public class NumTelefonico {
    private String etiqueta;
    private String numero;
    
    @Override
    public String toString() {
        return "NumTelefonico{" + "etiqueta=" + etiqueta + ", numero=" + numero + '}';
    }
    
    

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }   
}
