/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author luisg
 */
public class Foto {
    
    private String eqtiqueta;
    private String cadena;

    @Override
    public String toString() {
        return "Foto{" + "eqtiqueta=" + eqtiqueta + ", cadena=" + cadena + '}';
    }
    

    public String getEqtiqueta() {
        return eqtiqueta;
    }

    public void setEqtiqueta(String eqtiqueta) {
        this.eqtiqueta = eqtiqueta;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }
    
}
