/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analisisf;

import clases.Contacto;
import clases.Correo;
import clases.Direccion;
import clases.Foto;
import clases.NumTelefonico;
import escritura.Escritura;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Scanner;
import lectura.Lectura;

import manejador.SqlHelperContact2;
import operaciones.Operaciones;

/**
 *
 * @author luisg
 */
public class AnalisisF {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        // TODO code application logic here
        //*********DESCOMENTAR LAS SIGUIENTES LINEAS PARA EJECUTAR EL PARSER
        Lectura lectura = new Lectura();

        LinkedList<Contacto> listaContactosVcard = new LinkedList<Contacto>();
        listaContactosVcard = lectura.lectura();

        System.out.println("-----------MAIN PRINCIPAL--------");
        for (int i = 0; i < listaContactosVcard.size(); i++) {
            listaContactosVcard.get(i).setIdContacto("" + i);
            System.out.println("ID DEL CONTACTO: " + listaContactosVcard.get(i).getIdContacto());
            System.out.println("Apellido del contato: " + listaContactosVcard.get(i).getApellido());
            System.out.println("Nombre del contacto: " + listaContactosVcard.get(i).getNombre());
            System.out.println("Alias del contacto: " + listaContactosVcard.get(i).getAlias());

            System.out.println("Numeros del contacto: " + i);
            for (int j = 0; j < listaContactosVcard.get(i).getNumeros().size(); j++) {
                System.out.println(listaContactosVcard.get(i).getNumeros().get(j).toString());
            }
            System.out.println("Correos del contacto: " + i);
            for (int k = 0; k < listaContactosVcard.get(i).getCorreos().size(); k++) {
                System.out.println(listaContactosVcard.get(i).getCorreos().get(k).toString());
            }
            System.out.println("Direcciones del contacto: " + i);
            for (int l = 0; l < listaContactosVcard.get(i).getDirecciones().size(); l++) {
                System.out.println(listaContactosVcard.get(i).getDirecciones().get(l).toString());
            }
            System.out.println("Foto del contacto: " + listaContactosVcard.get(i).getFoto());

        }

        //Lista Contactos IncompletosCompletos
        LinkedList<Contacto> ListaContactosIncompletos = new LinkedList<>();

        //Lista Contactos Completos
        LinkedList<Contacto> ListaContactosCompletos = new LinkedList<>();

        //Lista Contactos SinImagen
        LinkedList<Contacto> ListaContactosSinImagen = new LinkedList<>();

        //Lista Contactos Repetidos
        LinkedList<Contacto> ListaContactosRepetidos = new LinkedList<>();

        
         
        System.out.println("-------------------------------------------------");

        Operaciones op = new Operaciones();

        //Metodo que guarda en otra lista los contactos con campos faltantes y los completos 
        
        op.ObtenerCompletosEincompletos(listaContactosVcard, ListaContactosIncompletos, ListaContactosCompletos);
       
        op.ObtenerSinImagen(listaContactosVcard, ListaContactosSinImagen);

        //Tamaño de la lista de contactosSinImagen
        //System.out.println("Cantidad de contactos sin Imagen ");
        // System.out.println(ListaContactosSinImagen.size());
        //Muestra la lista de contactosSinImagen
        //System.out.println("Cantidad de contactos sin imagen ");
        //System.out.println(ListaContactosSinImagen);
        //System.out.println("-------------------------------------------------");
        op.BuscarRepetidos(listaContactosVcard, ListaContactosRepetidos);
        //op.BuscarRepetidos3(ListaContactos, ListaContactosRepetidos);
        //op.Duplicate(ListaContactos);
        //op.RenoverDuplicados(ListaContactos,ListaContactosRepetidos);
        //Tamaño de la lista de contactosRepetidos
        //System.out.println(ListaContactosRepetidos.size());

        //Muestra la lista de contactosRepetidos
        //System.out.println(ListaContactosRepetidos);
        //System.out.println("-------------------------------------------------");
        //op.ObtenerCompletosEincompletos(listaContactosVcard,ListaContactosIncompletos,ListaContactosCompletos);
//public void Menu(LinkedList Lista, Scanner entrada){
        String Opcion = "";
        Scanner entrada = new Scanner(System.in);

        do {

            // Operaciones opcion = new Operaciones();
            System.out.println("\nIngrese el número de cada opción para ejecutar una orden.\n");
            System.out.println("1.Realizar Parseo de archivo.");
            System.out.println("2.Mostrar Contactos  Completos.");
            System.out.println("3.Mostrar Contactos  Incompletos.");
            System.out.println("4.Mostrar Contactos  Sin imagen.");
            System.out.println("5.Mostrar Contactos  Duplicados.");
            //System.out.println("6.Buscar contacto por Nombre.");
            System.out.println("6.Salir.");

            String Eleccion = entrada.next();

            switch (Eleccion) {

                case "1":
                    System.out.println("Realizando Parseo:");
                    /*listaContactosVcard = lectura.lectura();
            for(int i = 0; i<listaContactosVcard.size(); i++){
          //  System.out.println("Numeros del contacto: "+ i);
          int numEntero = i;    
          String numCadena= String.valueOf(numEntero);
          listaContactosVcard.get(i).setIdContacto(numCadena); 
        }
                     */
                    System.out.println("Parseo Completado");
                    System.out.println("Total de contacto ingresados:");
                    System.out.println(listaContactosVcard.size());

                    System.out.println("\n");
                    break;
                case "2":
                    System.out.println("Contactos Completos:");
                    System.out.println(ListaContactosCompletos.size());
                    System.out.println(ListaContactosCompletos);
                    System.out.println("\n");
                    break;
                case "3":
                    System.out.println("Contactos InCompletos:");
                    System.out.println(ListaContactosIncompletos.size());
                    System.out.println(ListaContactosIncompletos);
                    System.out.println("\n");
                    break;
                case "4":
                    System.out.println("Contactos Sin Imagen:");
                    //op.ObtenerSinImagen(listaContactosVcard,ListaContactosSinImagen);
                    System.out.println(ListaContactosSinImagen.size());
                    System.out.println(ListaContactosSinImagen);
                    System.out.println("\n");
                    break;
                case "5":
                    System.out.println("Contactos Duplicados:");
                    // op.BuscarRepetidos(listaContactosVcard,ListaContactosRepetidos);
                    System.out.println(ListaContactosRepetidos.size());
                    System.out.println(ListaContactosRepetidos);
                    System.out.println("\n");
                    break;

                case "6":
                    Opcion = "6";
                    break;
                default:
                    break;
            }

        } while (!"6".equals(Opcion));
        
        Escritura e = new Escritura();

    e.guardar (listaContactosVcard);

    //SqlHelperContact2 a = new SqlHelperContact2();
    //System.out.println("Ingresando un Contacto...");
    //a.insertarContacto(contacto);
    //System.out.println("Datos del contacto ingresado unido con la tabla de correos en la base de datos...");
    //a.imprimeContacto();

    }

    
}


