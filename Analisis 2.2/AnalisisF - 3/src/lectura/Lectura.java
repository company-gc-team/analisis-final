/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lectura;

import clases.Contacto;
import clases.Correo;
import clases.Direccion;
import clases.Foto;
import clases.NumTelefonico;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 *
 * @author luisg
 */
public class Lectura {

    public LinkedList<Contacto> lectura() throws IOException {
        int contador = 0;
        //cadena que contendra las lineas que vamos leyendo del archivo
        String cadenaLeida = "";
        //lista donde iremos agregando cada uno de los objetos tipo contacto
        LinkedList<Contacto> contactos = new LinkedList<Contacto>();
        FileReader fr;

        try {
            fr = new FileReader("contacts.vcf");
            BufferedReader archivoLectura = new BufferedReader(fr);

            cadenaLeida = archivoLectura.readLine();
            //System.out.println(cadenaLeida);
            String cad = "";

            //Creamos los primeros objetos antes de entrar al condicional
            Contacto contacto;
            LinkedList<Correo> correos;
            //Correo correo = new Correo();
            LinkedList<Direccion> direcciones;
            LinkedList<NumTelefonico> numeros;

            while (cadenaLeida != null) {
                
                String nom = null;
                String ap = null;
                String begin = "";
                while (begin != "BEGIN") {
                    //while (contador >= 0) {
                    String cadenaAux;
                    contacto = new Contacto();
                    correos = new LinkedList<Correo>();
                    //Correo correo = new Correo();
                    direcciones = new LinkedList<Direccion>();
                    numeros = new LinkedList<NumTelefonico>();
                    StringTokenizer st = new StringTokenizer(cadenaLeida, ":");
                    //etiqueta BEGIN
                    cadenaAux = st.nextToken();

                    if (cadenaAux.equals("BEGIN")) {

                        System.out.println("Etiqueta:" + cadenaAux);

                        //obtenemos su atributo
                        cadenaAux = st.nextToken();
                        System.out.println("Atributo:" + cadenaAux);
                        begin = cadenaAux;
                        //recuperamos la siguiente linea
                        cadenaLeida = archivoLectura.readLine();
                        StringTokenizer st2 = new StringTokenizer(cadenaLeida, ":");
                        //recuperamos el elemento
                        cadenaAux = st2.nextToken();
                        StringTokenizer s2 = new StringTokenizer(cadenaLeida, ":");
                        //etiqueta BEGIN
                        cadenaAux = st2.nextToken();

                    }

                    if (cadenaAux.equals("VERSION")) {
                        System.out.println("Etiqueta:" + cadenaAux);
                        //pasamos al siguiente que sera el atributo
                        cadenaAux = st.nextToken();
                        System.out.println("Atributo:" + cadenaAux);

                        //recuperamos la siguiente linea
                        cadenaLeida = archivoLectura.readLine();
                        StringTokenizer st3 = new StringTokenizer(cadenaLeida, ":");
                        //recuperamos el elemento
                        cadenaAux = st3.nextToken();
                        StringTokenizer s3 = new StringTokenizer(cadenaLeida, ":");

                        cadenaAux = st3.nextToken();

                    }
                    if (cadenaAux.equals("N")) {

                        System.out.println("Etiqueta:" + cadenaAux);

                        //pasamos al siguiente que sera el atributo
                        cadenaAux = st.nextToken();
                        //System.out.println("Cadena total:" + cadenaAux);
                        //dividimos en cadenas separadas por el identificador ;
                        String[] split = cadenaAux.split(";");
                        System.out.println("TAMANIO N:  " + split.length);
                        //System.out.println("Lognitud de la linea hasta la ultima palabra:" + split.length);
                        //contador total de esta linea
                        int cont1 = 5;
                        for (int i = 0; i < split.length; i++) {
                            //con este if solo mostramos los espacios vacias hasta que encuntre una cadena
                            if (split[i].equals("")) {
                                System.out.println("Entrooooooo");
                                if (i == 0) {
                                    contacto.setApellido(split[i]);
                                } else if (i == 1) {
                                    contacto.setNombre(split[i]);
                                }
                                //System.out.println("Atributo:Vacio");
                                //reducimos un campo de los faltantes (5 totales)
                                cont1 -= 1;
                            } else {
                                if (i == 0) {
                                    contacto.setApellido(split[i]);
                                    ap = split[i];
                                    System.out.println("Atributopppp:" + split[i]);
                                }
                                if (i == 1) {
                                    contacto.setNombre(split[i]);
                                    System.out.println("Atributoppppp:" + split[i]);
                                    nom = split[i];
                                    
                                }

                                //reducimos un campo de los faltantes (5 totales)
                                cont1 -= 1;
                            }
                        }
                        //por ultimo agregamos todos los campos faltantes
                        for (int j = 0; j < cont1; j++) {
                            System.out.println("Atributo: Vacio");
                        }
                        //avanzamos
                        cadenaLeida = archivoLectura.readLine();
                        StringTokenizer st4 = new StringTokenizer(cadenaLeida, ":");
                        //recuperamos el elemento
                        cadenaAux = st4.nextToken();
                        StringTokenizer s4 = new StringTokenizer(cadenaLeida, ":");
                        //etiqueta BEGIN
                        cadenaAux = st4.nextToken();

                    }
                    if (cadenaAux.equals("FN")) {
                        System.out.println("Etiqueta:" + cadenaAux);
                        //pasamos al siguiente que sera el atributo
                        cadenaAux = st.nextToken();
                        System.out.println("Atributo:" + cadenaAux);
                        contacto.setAlias(cadenaAux);
                        //recuperamos la siguiente linea
                        cadenaLeida = archivoLectura.readLine();
                        StringTokenizer st2 = new StringTokenizer(cadenaLeida, ";");
                        //recuperamos el elemento
                        cad = st2.nextToken();
                    }

                    while (cad.equals("TEL")) {
                        //if (cad.equals("TEL")) {

                        //CREAMOS UN NUEVO TELEFONO
                        NumTelefonico num = new NumTelefonico();

                        System.out.println("Etiqueta:" + cad);
                        //num.setEtiqueta(cad);
                        //pasamos al siguiente que sera el atributo
                        StringTokenizer st2 = new StringTokenizer(cadenaLeida, ":");
                        cad = st2.nextToken();
                        //remplazamos el etiqueta antigua por la cadena vacia
                        //cad = cad.replace("TEL;", "");
                        System.out.println("Tipo de Telefono:" + cad);
                        num.setEtiqueta(cad);
                        cad = st2.nextToken();
                        System.out.println("Atributo:" + cad);
                        num.setNumero(cad);

                        //avanzmos
                        cadenaLeida = archivoLectura.readLine();
                        StringTokenizer st7 = new StringTokenizer(cadenaLeida, ";");
                        //recuperamos el elemento
                        cad = st7.nextToken();//ESTA CADENA ES LA QUE CONTINUARA DE AQUI EN ADELANTE
                        System.out.println("Atributo:" + cad);
                        //lo agregamos a la lista de telefonos
                        numeros.add(num);

                    }
                    while (cad.equals("EMAIL")) {
                        //CREAMOS UN NUEVO OBJETO DE TIPO CORREO
                        Correo corr = new Correo();
                        System.out.println("Etiqueta:" + cad);
                        //pasamos al siguiente que sera el atributo
                        StringTokenizer st2 = new StringTokenizer(cadenaLeida, ":");
                        cad = st2.nextToken();
                        //remplazamos el etiqueta antigua por la cadena vacia
                        //cad = cad.replace("EMAIL;", "");
                        System.out.println("Tipo de EMAIL:" + cad);
                        corr.setEtiqueta(cad);
                        cad = st2.nextToken();
                        System.out.println("Atributo:" + cad);
                        corr.setCorreo(cad);

                        //avanzmos
                        cadenaLeida = archivoLectura.readLine();
                        StringTokenizer st7 = new StringTokenizer(cadenaLeida, ";");
                        //recuperamos el elemento
                        cad = st7.nextToken();
                        //Lo agregamos a la lista de mails antes de salir
                        correos.add(corr);
                    }
                    while (cad.equals("ADR")) {
                        //CREAMOS UNA Direccion NUEVO
                        Direccion dir = new Direccion();
                        System.out.println("Etiqueta:" + cad);
                        //pasamos al siguiente que sera el atributo
                        StringTokenizer st1 = new StringTokenizer(cadenaLeida, ":");
                        cad = st1.nextToken();
                        //remplazamos el etiqueta antigua por la cadena vacia
                        //cad = cad.replace("ADR;", "");
                        System.out.println("Tipo de DIRECCION:" + cad);
                        dir.setEtiqueta(cad);
                        cad = st1.nextToken();
                        System.out.println("Atributo:" + cad);
                        String[] split = cad.split(";");
                        System.out.println("TAMANIO DIR:  " + split.length);
                        if (split.length > 2) {
                            //si el arreglo tiene mas de dos campos agregamos desde el campo 3 que es donde empieza la calle
                            for (int i = 2; i < split.length; i++) {
                                if (i == 2) {
                                    dir.setCalle(split[i]);
                                    System.out.println("-----------ESTE DATO SE AGREGO AL OBJETO CONTACTO: " + dir.getCalle());
                                }
                                if (i == 3) {
                                    dir.setColonia(split[i]);
                                }
                                if (i == 4) {
                                    dir.setEstado(split[i]);
                                }
                                if (i == 5) {
                                    dir.setCp(split[i]);
                                }
                                if (i == 6) {
                                    dir.setPais(split[i]);
                                }
                            }

                        } else {
                            System.out.println("PASAMOS...");
                        }

                        //System.out.println("Lognitud de la linea hasta la ultima palabra:" + split.length);
                        //contador total de esta linea
                        int cont1 = 7;
                        for (int i = 0; i < split.length; i++) {
                            //con este if solo mostramos los espacios vacias hasta que encuntre una cadena
                            if (split[i].equals("")) {
                                System.out.println("Atributo:Vacio");
                                //reducimos un campo de los faltantes (5 totales)
                                cont1 -= 1;
                            } else {
                                System.out.println("Atributo:" + split[i]);
                                //reducimos un campo de los faltantes (5 totales)
                                cont1 -= 1;
                            }
                        }
                        //por ultimo agregamos todos los campos faltantes
                        for (int j = 0; j < cont1; j++) {
                            System.out.println("Atributo:  Vacio");
                        }
                        //avanzamos
                        cadenaLeida = archivoLectura.readLine();
                        StringTokenizer st4 = new StringTokenizer(cadenaLeida, ";");
                        //recuperamos el elemento
                        cad = st4.nextToken();
                        //lo agregamos a la lista de direcciones antes de salir
                        direcciones.add(dir);

                    }
                    if (cad.equals("PHOTO")) {
                        //Creamos un objeto de tipo foto
                        Foto foto = new Foto();
                        String encod;
                        System.out.println("Etiqueta:" + cad);

                        //pasamos al siguiente que sera el atributo
                        StringTokenizer st8 = new StringTokenizer(cadenaLeida, ":");
                        //en este momento toma la cadena desde FOTO por eso se avanza 2 veces
                        encod = st8.nextToken();

                        System.out.println("BASE:" + encod);
                        foto.setEqtiqueta(encod);
                        //pasamos al siguiente que sera el atributo
                        cad = st8.nextToken();

                        //avanzamos
                        String nuevo = "";

                        while (!cad.endsWith("=")) {
                            nuevo += cad + "\n";
                            cad = archivoLectura.readLine();
                        }
                        cad += archivoLectura.readLine();

                        System.out.println("CADENA FOTO:" + nuevo);
                        
                        
                        System.out.println(nuevo.length());

                        nuevo = nuevo + cad + "\n\n";
                        foto.setCadena(nuevo);
                        StringTokenizer st11 = new StringTokenizer(cadenaLeida, ":");
                        //en este momento toma la cadena desde FOTO por eso se avanza 2 veces
                        encod = st11.nextToken();
                        
                        //avanzmos
                        cadenaLeida = archivoLectura.readLine();
                        StringTokenizer st7 = new StringTokenizer(cadenaLeida, ";");
                        //recuperamos el elemento
                        cad = st7.nextToken();

                        System.out.println("Cadena Final:" + cad);
                        contacto.setFoto(foto);

                    }
                    if (cad.equals("END:VCARD")) {

                        //avanzmos
                        cadenaLeida = archivoLectura.readLine();
                        StringTokenizer st15 = new StringTokenizer(cadenaLeida, ":");
                        //recuperamos el elemento
                        cad = st15.nextToken();
                        cadenaAux = cad;

                        System.out.println("Cadena Final:" + cad);
                        begin = cad;
                        //avanzamos
                        cadenaLeida = archivoLectura.readLine();
                        StringTokenizer st4 = new StringTokenizer(cadenaLeida, ";");
                        cad = st4.nextToken();
                        cadenaAux = cad;

                        //Terminamos de crear el objeto con todos los campos recuperados
                        contacto.setNumeros(numeros);
                        contacto.setCorreos(correos);
                        contacto.setDirecciones(direcciones);
                        contacto.setNombre(nom);
                        contacto.setApellido(ap);
                        contacto.setAlias(nom+" "+ap);
                        contactos.add(contacto);
                        

                    }
                    System.out.println("CONTADOR DE CONTACTOS TOTALES:" + contactos.size());
                }
            }

            archivoLectura.close();
        } catch (FileNotFoundException e) {
            System.out.println("No se pudo encontrar el archivo");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("No se pudo leer del archivo");
            e.printStackTrace();
        } catch (NoSuchElementException e) {
            //por si no encuetra con el tokenizer solo pasamos
        } catch (NullPointerException e) {
            //por si no encuetra con el tokenizer solo pasamos
        }

        return contactos;

    }

}
