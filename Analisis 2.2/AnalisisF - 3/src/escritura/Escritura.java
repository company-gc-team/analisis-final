/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escritura;

import clases.Contacto;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

/**
 *
 * @author luisg
 */
public class Escritura {

    public void guardar(LinkedList<Contacto> contactos) {

        String nombreArchivo = "newContacts.vcf";
        try {

            FileWriter fw = new FileWriter(nombreArchivo, true);
            BufferedWriter archivoEscritura = new BufferedWriter(fw);
            String almacenar;
            Contacto contacto = new Contacto();

            for (int i = 0; i < contactos.size(); i++) {
                contacto = contactos.get(i);
                almacenar = "BEGIN:VCARD\n"
                        + "VERSION:2.1\n"
                        + "N:" + contacto.getApellido() + ";" + contacto.getNombre() + ";;;\n"
                        + "FN:" + contacto.getNombre() + " " + contacto.getApellido() + "\n";
                if (contacto.getNumeros().isEmpty()) {
                    //pasamos
                } else {
                    for (int j = 0; j < contacto.getNumeros().size(); j++) {
                        almacenar += contacto.getNumeros().get(j).getEtiqueta() + ":" + contacto.getNumeros().get(j).getNumero() + "\n";
                    }
                }
                if (contacto.getCorreos().isEmpty()) {
                    //pasamos
                } else {
                    for (int k = 0; k < contacto.getCorreos().size(); k++) {
                        almacenar += contacto.getCorreos().get(k).getEtiqueta() + ":" + contacto.getCorreos().get(k).getCorreo() + "\n";
                    }
                }
                if (contacto.getDirecciones().isEmpty()) {
                    //pasamos
                } else {
                    for (int m = 0; m < contacto.getDirecciones().size(); m++) {
                        almacenar += contacto.getDirecciones().get(m).getEtiqueta() + ":;;" + contacto.getDirecciones().get(m).getCalle()
                                + ";" + contacto.getDirecciones().get(m).getColonia() + ";" + contacto.getDirecciones().get(m).getEstado()
                                + ";" + contacto.getDirecciones().get(m).getCp() + ";" + contacto.getDirecciones().get(m).getPais() + "\n";
                    }
                }
                try {
                    if (contacto.getFoto().getCadena() == null) {
                        //pasamos
                    } else {
                        almacenar += contacto.getFoto().getEqtiqueta() + ":" + contacto.getFoto().getCadena();
                    }
                } catch (NullPointerException e) {

                }

                almacenar += "END:VCARD";
                archivoEscritura.write(almacenar);
                archivoEscritura.newLine();
                archivoEscritura.flush();

            }

            archivoEscritura.close();

        } catch (IOException e) {

            System.out.println("Error al escribir en el archivo");
            e.printStackTrace();

        }

    }

}
