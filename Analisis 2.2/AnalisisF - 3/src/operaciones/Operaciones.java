
package operaciones;

import clases.Contacto;
import clases.Correo;
import clases.Direccion;
import clases.NumTelefonico;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;



/**
 * @author Sergio Castillo 2182000613
 */
public class Operaciones {
    
    public LinkedList insertarContacto(LinkedList listaContactos, Contacto contac){
        
        listaContactos.add(contac);
        return listaContactos;
    
    } 
    
    
    
    public Contacto regresarContacto(LinkedList listaContacto, int pos){
        
        Contacto cont= (Contacto)listaContacto.get(pos);
        return cont;
    
    }
    
    public int buscarContacto(LinkedList listaContactos, String nombre){
        
        int pos=-1;
        
        for(int i=0;i<listaContactos.size();++i){
            
            Contacto a=(Contacto)listaContactos.get(i);
            
            if(a.getNombre().compareTo(nombre)==0){
                pos=i;
                break;
            }
        
        }
        return pos;
    }
       
   
    public LinkedList borrarContacto(LinkedList listaContactos, String cont){
        
        int posBorrar = buscarContacto(listaContactos,cont);
        
        if(posBorrar == -1){
            
            System.out.println("No existe en la lista " + cont );
        
        }else{
            
            listaContactos.remove(posBorrar);
            System.out.println("Los datos del contacto se han eliminado.");
        }
        
        return listaContactos;
    
    }
    
    
    // Metodos para clasificar Contactos
   
     
    public LinkedList BuscarRepetidos(LinkedList <Contacto> lista1,LinkedList lista2){
    int flag=0;
        for(int i=0;i<lista1.size();i++){
            
            Contacto a=(Contacto)lista1.get(i);
            for(int j=0;j<lista1.size();j++){
            Contacto b=(Contacto)lista1.get(j);
             if(i!=j && a.getApellido()== b.getApellido()){ 
                flag= flag +1;
                //System.out.println("Flag= a "+ flag);
                //System.out.println(" coinciden ");
            }
             
             if(i!=j && a.getNombre()== b.getNombre()){ 
                flag=flag +1;
                //System.out.println("Flag= a "+ flag);
                //System.out.println(" coinciden ");
            }
             
               if(i!=j && a.getAlias()== b.getAlias()){ 
                flag=flag +1;
                //System.out.println("Flag= a "+ flag);
                //System.out.println(" coinciden ");
            }
              //Recorre Lista de Contactos 
           /*for(int k=0;k<a.getNumeros().size();++k){
                NumTelefonico n=(NumTelefonico)a.getNumeros().get(k);
                
                for(int l=0;l<b.getNumeros().size();++l){
                NumTelefonico n1=(NumTelefonico)b.getNumeros().get(l);
                    
                    if(k!=l && n.getNumero()== n1.getNumero()){
                  //lista2.add(lista1.get(i));
                flag=+1;  
                System.out.println("Flag"+ flag);
                System.out.println(" coinciden ");
            }
          
            if(n.getEtiqueta()== n1.getEtiqueta()){
                  //lista2.add(lista1.get(i));
                flag=+1;    
               System.out.println("Flag"+ flag);
                System.out.println(" coinciden ");
            }
                }
                    
               }*/
            }
            
            if (flag == 3) {
               lista2.add(lista1.get(i));
                //System.out.println("Exiten Reptidos");
                //System.out.println("Flag=" + flag);
           }
            //System.out.println("Flag=" + flag);
            flag = 0;
        }
        
        
    return lista2;
    }
     
     public LinkedList ObtenerCompletosEincompletos(LinkedList <Contacto> lista1 ,LinkedList lista2 ,LinkedList lista3){
       int flag = 0;
        for(int i=0;i<lista1.size();++i){
            
            Contacto a=(Contacto)lista1.get(i);
            
            if(a.getApellido()== null){ 
                flag=1;    
            }
          
            if (a.getNombre()==null){
             
                flag=1; 
                  
            }
            
             if (a.getAlias()==null){
                flag=1;  
            }
            
            // recorre la lista de numeros en busca de vacios
            for(int j=0;j<a.getNumeros().size();++j){
                NumTelefonico n=(NumTelefonico)a.getNumeros().get(j);
            if(n.getNumero()==null){ 
                flag=1; 
            }
          
            if(n.getEtiqueta()==null){
                flag=1;    
            }
            }
            
             // recorre la lista de correos en busca de vacios
            for(int k=0;k<a.getCorreos().size();++k){
                Correo n=(Correo)a.getCorreos().get(k);
            if(n.getCorreo()== null){
                  
                flag=1;   
              
            }
            
            if(n.getEtiqueta()==null){
                  
             flag=1;  
             
            }
           
            }
          
            // recorre la lista de direccion en busca de vacios
            for(int l=0;l<a.getDirecciones().size();++l){
                Direccion n=(Direccion)a.getDirecciones().get(l);
                
            if(n.getEtiqueta()==null){
                  
             flag=1;    
             
            }
            
            
            if(n.getCalle()==null){
                  
             flag=1;  
           
            }
           
            
            if(n.getColonia()==null){
                  
             flag=1;  
             
            }
           
            
            if(n.getEstado()==null){
                
             flag=1;  
            
            }
          
            if(n.getCp()==null){
                  
             flag=1;  
            
            }
          
            if(n.getPais()==null){
                  
             flag=1;  
             //System.out.println("bandera:" + flag );
            }
           
            }
          
            if(a.getFoto()== null){ 
                 
             flag=1;
             
            }
           
            
            //if(a.getFoto().getCadena()== null){ 
                
             //flag=1; 
             
            //}
            
           if(flag !=0){
            lista1.get(i).setCompleto(false);
            lista2.add(lista1.get(i));
     
             //a.setCompleto(false);
            }
            
            if(flag == 0){
            lista1.get(i).setCompleto(true);
            lista3.add(lista1.get(i));
             //a.setCompleto(true);
            }
   
            flag = 0;
            
        }
            
        return lista2;    
    }
    
   
     
     public LinkedList ObtenerSinImagen(LinkedList <Contacto> lista1 ,LinkedList lista2){
       int flag = 0;
        for(int i=0;i<lista1.size();++i){
            
            Contacto a=(Contacto)lista1.get(i);
           
            if(a.getFoto()== null){ 
                 
             flag=1;
             
            }
        
            //if("vacio".equals(a.getFoto().getCadena())){ 
                 
             //flag=1; 
             
           // }
            
           
            if(flag ==1){
            lista2.add(lista1.get(i));
            }
            
            flag = 0;
            
        }
            
        return lista2;    
    }
     
     
    
    
    public LinkedList Duplicate (LinkedList <Contacto> list1) {

    LinkedList <Contacto> duplicatedObjects = new LinkedList<Contacto>();
    Set<Contacto> set = new HashSet<Contacto>() {
    @Override
    public boolean add(Contacto contacto) {
        if (contains(contacto)) {
            duplicatedObjects.add(contacto);
        }
        return super.add(contacto);
    }
    };
   for (Contacto t : list1) {
        set.add(t);
    }
   System.out.println(duplicatedObjects);
    return duplicatedObjects;
    
}



    
    public LinkedList BuscarRepetidos2(LinkedList <Contacto> lista1,LinkedList lista2){
    //int flag1=0;
    //int flag2=0;
        for(int i=0;i<lista1.size();i++){
            int flag1=0;
            int flag2=0;
            Contacto a=(Contacto)lista1.get(i);
            for(int j=0;j<lista1.size();j++){
                //int flag1=0;
            Contacto b=(Contacto)lista1.get(j);
             if(i!=j && a.getApellido()== b.getApellido()
                && a.getNombre()== b.getNombre() && a.getAlias()== b.getAlias() ){ 
                flag1= 10;
                System.out.println("Flag1= aaaaa "+ flag1);
                System.out.println(" coincidennn ");
            }
             
            //flag1=0;
            }
            //flag1 = 0;
        
        for(int k=0;k<lista1.get(i).getNumeros().size();k++){
            //Contacto b=(Contacto)lista1.get(i);
            //int flag2=0;
           //NumTelefonico n=(NumTelefonico)a.getNumeros().get(k);
            for(int m=0;m<lista1.get(i).getNumeros().size();m++){
           //NumTelefonico n1=n=(NumTelefonico)a.getNumeros().get(m);
             if(k!=m && lista1.get(i).getNumeros().get(k).getEtiqueta()==lista1.get(i).getNumeros().get(m).getEtiqueta()){
                //&& n.getNumero()==n1.getNumero()){ 
                flag2= 15;
                System.out.println("Flag2= AAAA "+ flag2);
                System.out.println(" coinciden ");
            }
             
             //flag2=0;
            }
        }
            //flag2 = 0;
       
            //if (flag1 ==1  && flag2 == 1 ) {
              // lista2.add(lista1.get(i));
                //System.out.println("Prueba");
               //System.out.println("Flag=" + flag);
           //}
            
            
        }
            //System.out.println("Flag=" + flag);
           //flag 
        
    return lista2;
    }
    
     public LinkedList BuscarRepetidos3(LinkedList <Contacto> lista1,LinkedList lista2){
    int flag1=0;
    int flag2=0;
    
        for(int i=0;i<lista1.size();i++){
            
            Contacto a=(Contacto)lista1.get(i);
            for(int j=0;j<lista1.size();j++){
            Contacto b=(Contacto)lista1.get(j);
             if(i!=j && a.getApellido() == b.getApellido()
                && a.getNombre()== b.getNombre()
                && a.getAlias() == b.getAlias()){ 
                flag1=10;
                System.out.println("Flag= aaa "+ flag1);
                System.out.println(" coinciden ");
             }
            
              //Recorre Lista de Contactos 
              for(int k=0;k<lista1.get(i).getNumeros().size();k++){
            
                 for(int m=0;m<lista1.get(i).getNumeros().size();m++){
           
                     if(k!=m && lista1.get(i).getNumeros().get(k).getEtiqueta()==lista1.get(i).getNumeros().get(m).getEtiqueta()){
                       //&& n.getNumero()==n1.getNumero()){ 
                       flag2= 15;
                       System.out.println("Flag2= AAA "+ flag2);
                       System.out.println(" coinciden ");
                         
                    }
             
            }
              
           }
    }
            System.out.println("suma de flags = " + (flag1 + flag2));
              System.out.println("Flag1= es "+ flag1);
              System.out.println("Flag2= es "+ flag2);
              if (flag1 == 10 && flag2== 15) {
                    lista2.add(lista1.get(i));
                    System.out.println("repetodos hasta numeros");
                    //System.out.println("suma de flags = " + (flag1 + flag2)) ;
        }
            
           flag1 = 0;
           flag2 = 0;
        }
        
        
    return lista2;
    }
   
   

 }
           
             
        